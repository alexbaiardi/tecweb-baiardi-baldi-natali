<?php
  require_once("setup.php");
  if (!isUserLoggedIn()) {
    setcookie("login-carrello","true",time() + (60 * 60 * 24),"/");
    header("Location: login.php");
  } else {
    if(isUserAdmin()){
      header("Location: index.php");
    }
  }

  $templateParams["titolo"]="Fitonline - Riepilogo carrello";
  $templateParams["js"]=array("js/quantita.js","js/modificaCarrello.js");
  $templateParams["mainTemplate"]="mainCarrello.php";

  $carrello=$dbCarrello->getCarrello($_SESSION["id"]);
  $templateParams["carrello"]=array();
  $subtotale=0;
  foreach ($carrello as $idProdotto => $quantita) {
    $prodotto=$dbProdotti->getProdottoById($idProdotto);
    $quantita=min($quantita,$prodotto["quantitaDisponibile"]);
    $utente=$dbh->getDatiUtente($_SESSION["id"]);
    if($quantita==0 || ($prodotto["patentino"] && !$utente["patentino"])){
      $dbCarrello->rimuoviProdotto($_SESSION["id"],$idProdotto);
    } else {
      $dbCarrello->modificaQuantitaProdotto($quantita,$_SESSION["id"],$idProdotto);
      $prodotto["quantita"]=$quantita;
      $subtotale+=$prodotto["prezzo"]*$quantita;
      array_push($templateParams["carrello"],$prodotto);
    }
  }
  $templateParams["subtotale"]=number_format($subtotale,2,"."," ");
  require 'template/base.php';
?>
