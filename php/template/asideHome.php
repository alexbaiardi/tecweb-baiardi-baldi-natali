<aside>
  <div class="container">
    <header>
      <h2 class="text-center fs-3">Cerca prodotto</h2>
    </header>
    <form class="form" action="./ricerca.php" method="get">
      <div class="row">
        <div class="col-md-6 pt-5">
          <label for="nomeProdotto" class="visually-hidden">Nome Prodotto</label>
          <input type="text" id="nomeProdotto" name="nomeProdotto" class="form-control" placeholder="Nome Prodotto"/>
        </div>
        <div class="col-md-6 pt-5">
          <label for="marca" class="visually-hidden">Seleziona marca</label>
          <select id="marca" class="form-select" name="marca">
            <option disabled selected>Seleziona marca...</option>
            <?php foreach ($templateParams["marche"] as $marca): ?>
              <option value="<?php echo $marca["id"]; ?>"><?php echo $marca["nome"]; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="text-center mt-4">
          <button class="btn btn-success" type="submit"><span class="fa fa-search me-2"></span> Trova prodotto</button>
        </div>
      </div>
    </form>
  </div>
</aside>
