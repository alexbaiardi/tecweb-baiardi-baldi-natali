<div class="col-md-3 bg-light offset-lg-1 mt-3 order-md-1">
  <aside>
    <div class="container">
      <section>
        <header>
          <h3 class="text-center my-5">Cerca prodotto</h3>
        </header>
        <form class="form" action="./ricerca.php" method="get">
          <div class="row">
            <label for="nomeProdottoCerca" class="visually-hidden">Nome Prodotto</label>
            <input type="text" id="nomeProdottoCerca" name="nomeProdotto" class="form-control" placeholder="Nome Prodotto"/>
          </div>
          <div class="row mt-4">
            <label for="marca" class="visually-hidden">Seleziona marca</label>
            <select id="marca" name="marca" class="form-select">
              <option disabled selected>Seleziona marca...</option>
              <?php foreach ($templateParams["marche"] as $marca): ?>
                <option value="<?php echo $marca["id"]; ?>"><?php echo $marca["nome"]; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="row">
            <div class="text-center my-4">
              <button class="btn btn-success" type="submit"><span class="fa fa-search me-2"></span> Trova prodotto</button>
            </div>
          </div>
        </form>
      </section>
      <section>
        <h3 class="text-center my-4">Categorie</h3>
        <nav class=" list-group text-center">
          <?php foreach ($templateParams["categorie"] as $categoria): ?>
            <a class="list-group-item list-group-item-action " href="./categoria.php?idCategoria=<?php echo $categoria["id"];  ?>"><?php echo $categoria["nome"]; ?></a>
          <?php endforeach; ?>
        </nav>
      </section>
    </div>
  </aside>
</div>
