<section>
  <header>
    <h2 class="text-center mt-3">Riepilogo Ordini</h2>
  </header>
  <?php foreach ($datiOrdini as $ordine):?>
    <div class="card col-md-10 offset-md-1 mt-4">
      <div class="card-body row riepilogo">
        <div class="col-8 col-sm-12 col-md-3 offset-3 offset-md-1">
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0">Numero ordine: <?php echo $ordine["id"] ?></p>
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0 mt-sm-2">Totale : <?php echo number_format($ordine["totale"], 2, '.', ' ')?> €</p>
        </div>
        <div class="col-8 col-sm-12 col-md-3 offset-3 offset-md-1">
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0">Data ordine : <?php echo $ordine["data"] ?></p>
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0 mt-sm-2">Numero articoli : <?php echo $ordine["numArticoli"]?></p>
        </div>
        <div class="col-8 col-sm-12 col-md-3 offset-3 offset-md-1">
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0">Stato : <?php echo $ordine["stato"] ?></p>
          <button class="btn btn-success dettaglio form-check form-check-inline col-sm-4 col-md-12 mt-sm-2" type="button">Mostra dettaglio</button>
        </div>
      </div>
      <div class="card-body row visualizzaDettaglio">
        <hr class="col-10 offset-1">
        <?php $dettaglio = $dbOrdini->getDettaglioOrdine($ordine["id"]); ?>
        <?php foreach ($dettaglio as $rigaOrdine): ?>
          <div class="col-11 col-md-4 offset-3 offset-md-1">
            <p>Prodotto : <?php echo $rigaOrdine["nome"] ?></p>
          </div>
          <div class="col-11 col-md-3 offset-3 offset-md-0">
            <p>Prezzo unitario : <?php echo number_format($rigaOrdine["prezzo"],2,'.',' ')?> €</p>
          </div>
          <div class="col-11 col-md-3 offset-3 offset-md-1">
            <p>Quantità : <?php echo $rigaOrdine["quantita"] ?></p>
          </div>
          <hr class="col-10 offset-1">
        <?php endforeach; ?>
      </div>
    </div>
  <?php endforeach; ?>
</section>
