<div class="row mt-md-5">
  <div class="col-12 col-md-10 offset-md-1">
    <h2 class="text-center mt-2 mb-4">Registrazione</h2>
    <?php if(isset($erroreInserimento)): ?>
    <span class="alert alert-danger delay-alert p-0" role="alert"><?php echo $erroreInserimento; ?></span>
    <?php endif; ?>
    <form class="row" action="#" method="POST" enctype="multipart/form-data" id="registrazione">
      <div class="col-12 col-md-6">
        <label for="nome" class="form-label">Nome *</label>
        <input type="text" class="form-control" id="nome" name="nome" minlength="2" required/>
      </div>
      <div class="col-12 col-md-6 mt-3 mt-md-0">
        <label for="cognome" class="form-label">Cognome *</label>
        <input type="text" class="form-control" id="cognome" name="cognome" minlength="2" required/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <label for="email" class="form-label">Email address *</label>
        <input type="email" class="form-control" name="email" id="email" required/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <label for="emailConf" class="form-label">Conferma email *</label>
        <input type="email" class="form-control" id="emailConf" required/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <label for="password" class="form-label">Password *</label>
        <input type="password" class="form-control" name="password" id="password" minlength="8" required/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <label for="passwordConf" class="form-label">Conferma password *</label>
        <input type="password" class="form-control" id="passwordConf" minlength="8" required/>
      </div>
      <div class="col-12 col-md-7 col-xl-5 mt-3">
        <fieldset>
          <legend class="col-5 col-lg-4 fs-6">Hai il patentino? *</legend>
          <div class="form-check form-check-inline col-1">
            <input class="form-check-input" type="radio" name="patentino" id="si"/>
            <label class="form-check-label" for="si">Si</label>
          </div>
          <div class="form-check form-check-inline col-1">
            <input class="form-check-input" type="radio" name="patentino" id="no"/>
            <label class="form-check-label" for="no">No</label>
          </div>
        </fieldset>
      </div>
      <div class="row">
        <div class="col-12 col-md-6 mt-3" id="numPat">
          <label for="idPat" class="form-label">Numero patentino *</label>
          <input type="text" class="form-control" name="idPatentino" id="idPat" minlength="5"/>
        </div>
        <div class="col-12 col-md-6 mt-3" id="uploadPat">
          <label for="pat" class="form-label">Carica patentino *</label>
          <input class="form-control" type="file" name="imgPatentino" id="pat"/>
        </div>
      </div>
      <button type="submit" class="btn btn-success mt-4 col-12 px-0 col-md-4 offset-md-8">Registrati</button>
    </form>
  </div>
</div>
