<div class="col-md-9 col-lg-7 order-md-2 d-flex align-self-stretch">
  <main>
    <section>
      <div class="row">
        <div class="col-sm-7 position-relative">
          <div class="ratio ratio-4x3">
            <img class="img-fluid img-fit-product mt-2" src="<?php echo IMG_PRODOTTI_DIR.$prodottoSelezionato["immagine"] ?>" alt="" />
          </div>
          <?php
            if ($prodottoSelezionato["patentino"]) {
              echo "<span class=\"badge position-absolute top-0 end-0 bg-danger d-flex col-4 m-4\">Patentino</span>";
            }
          ?>
        </div>
        <div class="col-sm-5">
          <h2 class="mt-3"><?php echo $prodottoSelezionato["nome"] ?></h2>
          <strong>Prezzo: <?php echo $prodottoSelezionato["prezzo"] ?> €</strong>

          <?php  if($prodottoSelezionato["quantitaDisponibile"] != 0) : ?>
            <form class="form carrello" action="aggiungiAlCarrello.php" method="post">
              <input type="hidden" name="idProdotto" value="<?php echo $prodottoSelezionato["id"]?>">
              <label for="quantita_<?php echo $prodottoSelezionato["id"]?>" class="form-label col-5 mt-3">Quantità:</label>
              <div class="col-7 float-end mt-2">
                <div class="input-group">
                  <button type="button" class="diminuisci btn btn-success btn-number">-</button>
                  <input type="number" id="quantita_<?php echo $prodottoSelezionato["id"]?>" name="quantita" class="form-control input-number" min="1" max="<?php echo $prodottoSelezionato["quantitaDisponibile"]; ?>" value="1"/>
                  <button type="button" class="aumenta btn btn-success btn-number">+</button>
                </div>
              </div>
              <div class="info mt-2">
                <p><span class="fa fa-check-circle text-success me-2"></span>Prodotto disponibile</p>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-success text-align-center align-items-center mt-4"><span class="fa fa-shopping-cart me-2"></span>Aggiungi al carrello</a>
              </div>
            </form>
            <?php else: ?>
              <div class="mt-2">
                  <p><span class="fa fa-check-circle text-danger me-2"></span>Prodotto non disponibile</p>
              </div>
            <?php endif ?>
          <?php if(isset($prodottoSelezionato["sitoWeb"])): ?>
            <div class="text-center">
              <a class="btn btn-success text-align-center align-items-center mt-4 col-11" href="<?php echo $prodottoSelezionato["sitoWeb"]; ?>" role="button"><span class="fa fa-info me-3"></span>Sito ufficiale</a>
            </div>
          <?php endif; ?>
        </div>
    </div>
    <div class="mt-4 col-10 offset-1">
      <h3 class="text-center">Descrizione del prodotto</h3>
      <p><?php echo $prodottoSelezionato["descrizione"]; ?></p>
    </div>
  </section>
  </main>
</div>
