<main>
  <section>
    <header>
      <h2 class="text-center pt-5">Categorie</h2>
    </header>
    <div class="container-fluid">
      <div class="album py-5">
        <div class="row d-flex">
          <?php foreach ($templateParams["categorie"] as $categoria): ?>
            <div class="col-sm-6 col-lg-4 d-flex align-self-stretch">
              <div class="card mb-4 shadow-sm d-flex flex-fill">
                <div class="ratio ratio-16x9 text-center">
                  <img class="img-fluid img-fit" src="<?php echo IMG_CATEGORIE_DIR.$categoria["immagine"]; ?>" alt=""/>
                </div>
                <div class="card-body">
                  <h3 class="card-title text-center"><?php echo $categoria["nome"]; ?></h3>
                  <p class="card-text"><?php echo $categoria["anteprimaDescrizione"]; ?></p>
                  <div class="text-center">
                    <a class="btn btn-success" href="./categoria.php?idCategoria=<?php echo $categoria["id"]; ?>" role="button">Visualizza Categoria</a>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container-fluid">
      <header>
        <h2 class="text-center">Prodotti più venduti</h2>
      </header>
      <div class="album py-5 ">
        <?php if(isUserLoggedIn() && isUserAdmin()): ?>
        <div class="modal fade" id="modalCancelProduct" tabindex="-1" aria-labelledby="titoloModal" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title" id="titoloModal" >Cancellazione</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
                <div class="modal-body">
                  Sei sicuro di voler cancellare il prodotto selezionato?
                </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annulla</button>
                <a href="" id="cancelProduct" class="btn btn-primary">Cancella</a>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
        <div class="row">
          <?php foreach ($templateParams["prodotti"] as $prodotto): ?>
            <div class="col-sm-6 col-lg-3 d-flex align-self-stretch ">
              <?php
                if (isUserLoggedIn() && isUserAdmin()) {
                  require("template/cardProdottoAdmin.php");
                } else {
                  require("template/cardProdotto.php");
                }
              ?>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </section>
</main>
