<div class="col-md-9 col-lg-7 order-md-2 d-flex align-self-stretch">
  <main class="col">
    <header>
      <h2 class="text-center mt-4"><?php echo $categoriaSelezionata["nome"]; ?></h2>
    </header>
    <?php if ($templateParams["paginaCorrente"]==1): ?>
      <article>
        <div class="col-xl-8 offset-xl-2">
          <div class="mt-3 ratio ratio-21x9">
              <img class="img-fluid img-fit" src="<?php echo IMG_CATEGORIE_DIR.$categoriaSelezionata["immagine"]; ?>" alt="" />
          </div>
        </div>
        <div class="mt-2 p-2">
            <p><?php echo $categoriaSelezionata["descrizione"]; ?></p>
        </div>
      </article>
    <?php endif; ?>
    <section>
      <div class="album py-5 ">
        <?php if(isUserLoggedIn() && isUserAdmin()): ?>
          <div class="modal fade" id="modalCancelProduct" tabindex="-1" aria-labelledby="titoloModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h3 class="modal-title" id="titoloModal">Cancellazione</h3>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  Sei sicuro di voler cancellare il prodotto selezionato?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annulla</button>
                  <a href="" id="cancelProduct" class="btn btn-primary">Cancella</a>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <div class="row">
          <?php foreach ($templateParams["prodotti"] as $prodotto): ?>
            <div class="col-12 col-sm-6 col-xl-4 d-flex align-self-stretch ">
              <?php
                if (isUserLoggedIn() && isUserAdmin()) {
                  require("template/cardProdottoAdmin.php");
                } else {
                  require("template/cardProdotto.php");
                }
              ?>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
      <?php require("paginazione.php");?>
    </section>
  </main>
</div>
