<div class="col-md-9 col-lg-7 order-md-2 d-flex align-self-stretch">
  <main class="col">
    <section>
      <header>
        <h2 class="text-center">Modifica articolo</h2>
      </header>
      <?php if(isset($templateParams["messaggio"] )):
       if($templateParams["messaggio"]["tipo"] == "Successo"): ?>
        <div class="mt-2">
            <p><span class="fa fa-check-circle text-success me-2"></span> <?php echo $templateParams["messaggio"]["tipo"].":" ?> <?php echo $templateParams["messaggio"]["testo"] ?></p>
        </div>
        <?php else: ?>
        <div class="mt-2">
            <p><span class="fa fa-exclamation-triangle text-danger me-2"></span><?php echo $templateParams["messaggio"]["tipo"].":" ?></p>
            <ul>
              <?php foreach ($templateParams["messaggio"]["testo"] as $info): ?>
                <li><?php echo $info ?></li>
              <?php endforeach; ?>
            </ul>
        </div>
      <?php endif;
      endif;
      ?>
        <form  action="./operazioniProdotto.php?action=2" method="post" enctype="multipart/form-data">
          <input type="hidden" name="idProd" value="<?php echo $prodottoSelezionato["id"]; ?>"/>
        <div class="row">
          <div class="col-sm-4 position-relative">
            <div class="ratio ratio-4x3">
              <img class="img-fluid img-fit-product mt-2" src="<?php echo IMG_PRODOTTI_DIR.$prodottoSelezionato["immagine"] ?>" alt=""/>
            </div>
          </div>
          <div class="col-sm-7 mt-5">
              <label for="immagineProdotto" class="form-label">Inserisci nuova immagine</label>
              <input class="form-control" id="immagineProdotto" type="file" name="immagineProdotto" />
          </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-6 mt-3">
          <label for="nomeProdotto" class="form-label">Nome prodotto prodotto</label>
          <input class="form-control" type="text" name="nomeProdotto" id="nomeProdotto" value="<?php echo $prodottoSelezionato["nome"]; ?>" required/>
        </div>
        <div class="col-12 col-md-6 mt-3">
          <label for="quantitaProdotto" class="form-label">Modifica quantita</label>
          <input class="form-control" type="number" name="quantitaProdotto" id="quantitaProdotto" value="<?php echo $prodottoSelezionato["quantitaDisponibile"]; ?>"required />
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-6 mt-3">
          <label for="prezzoProdotto" class="form-label">Prezzo</label>
          <input class="form-control" type="number"  name="prezzoProdotto" id="prezzoProdotto" value="<?php echo $prodottoSelezionato["prezzo"]; ?>" required />
        </div>
        <div class="col-12 col-md-6 mt-3">
          <label for="sitoWeb" class="form-label">Sito Web</label><small class="text-muted ms-2">(Opzionale)</small>
          <input class="form-control" type="url"  name="sitoWeb" id="sitoWeb" value="<?php echo $prodottoSelezionato["sitoWeb"]; ?> "/>
        </div>
      </div>
      <div class="row mt-5">
        <fieldset>
          <legend class="fs-6 col-3">Patentino</legend>
          <div class="col-3 form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radioPat" id="radioPatSi" value="1" <?php if($prodottoSelezionato["patentino"]){ echo "checked";}?>/>
            <label class="form-check-label" for="radioPatSi">Si</label>
          </div>
          <div class="col-3 form-check form-check-inline">
            <input class="form-check-input" type="radio" name="radioPat" id="radioPatNo" value="0" <?php if(!$prodottoSelezionato["patentino"]){ echo "checked";}?> />
            <label class="form-check-label" for="radioPatNo">No</label>
          </div>
        </fieldset>
      </div>
    <div class="col-md-8 offset-md-2 mt-4 text-center">
        <label class="tcol-12 text-center" for="descrizioneProdotto">Descrizione prodotto</label>
        <textarea name="desc" id="descrizioneProdotto" class="form-control" required> <?php echo $prodottoSelezionato["descrizione"]; ?></textarea>
    </div>
      <div class="row mt-5">
        <div class="col text-center">
          <button type="button" class="btn btn-danger" name="button" data-bs-toggle="modal" data-bs-target="#CancelModal">Cancella</button>
        </div>
        <div class="modal fade" id="CancelModal" tabindex="-1" aria-labelledby="titoloModal" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title" id="titoloModal" >Cancellazione</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                Sei sicuro di voler cancellare il prodotto selezionato?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Annulla</button>
                <a href="./operazioniProdotto.php?action=1&id=<?php echo $prodottoSelezionato["id"]; ?>" id="cancelButton" class="btn btn-primary">Cancella</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col text-center">
          <button type="submit" class="btn btn-success" name="modifica" >Conferma Modifiche</button>
        </div>
      </div>
    </form>
  </section>
  </main>
</div>
