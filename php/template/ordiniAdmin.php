<section>
  <header>
    <h2 class="text-center mt-3">Riepilogo Ordini</h2>
  </header>
  <?php foreach ($datiOrdini as $ordine):?>
    <div class="card col-md-10 offset-md-1 my-4">
      <div class="card-body row riepilogo">
        <div class="col-8 col-sm-12 col-md-3 offset-3 offset-md-1">
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0">Numero ordine: <?php echo $ordine["id"] ?></p>
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0 mt-sm-2">Totale : <?php echo number_format($ordine["totale"], 2, '.', ' ')?> €</p>
        </div>
        <div class="col-8 col-sm-12 col-md-3 offset-3 offset-md-1">
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0">Data ordine : <?php echo $ordine["data"] ?></p>
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0 mt-sm-2">Numero articoli : <?php echo $ordine["numArticoli"]?></p>
        </div>
        <div class="col-8 col-sm-12 col-md-3 offset-3 offset-md-1">
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0">Cliente : <?php echo $ordine["idCliente"] ?></p>
          <p class="form-check form-check-inline col-12 col-sm-4 col-md-12 p-0">Stato : <?php echo $ordine["stato"] ?></p>
        </div>
        <div class="col-10 col-sm-8 col-md-12 offset-2 offset-sm-3 offset-md-0">
          <div class="row">
            <button class="btn btn-success dettaglio form-check form-check-inline col-10 col-md-3 offset-md-4 mt-4" type="button">Mostra dettaglio</button>
            <?php if ($ordine["stato"] === "confermato"): ?>
              <form class="form col-10 col-md-3 mt-4 offset-md-1 p-0" action="#" method="POST">
                <input type="hidden" name="idOrdine" value="<?php echo $ordine["id"] ?>"/>
                <button type="submit" class="btn btn-success col-12">Spedisci</button>
              </form>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="card-body row visualizzaDettaglio">
        <hr class="col-10 offset-1">
        <?php $dettaglio = $dbOrdini->getDettaglioOrdine($ordine["id"]); ?>
        <?php foreach ($dettaglio as $rigaOrdine): ?>
          <div class="col-11 col-md-4 offset-3 offset-md-1">
            <p>Prodotto : <?php echo $rigaOrdine["nome"] ?></p>
          </div>
          <div class="col-11 col-md-3 offset-3 offset-md-0">
            <p>Prezzo unitario : <?php echo number_format($rigaOrdine["prezzo"],2,'.',' ')?> €</p>
          </div>
          <div class="col-11 col-md-3 offset-3 offset-md-1">
            <p>Quantità : <?php echo $rigaOrdine["quantita"] ?></p>
          </div>
          <hr class="col-10 offset-1">
        <?php endforeach; ?>
      </div>
    </div>
  <?php endforeach; ?>
  <?php require("paginazione.php");?>
</section>
