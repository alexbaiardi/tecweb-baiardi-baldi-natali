<div class="row mt-md-5">
  <div class="col-12 col-md-4 offset-md-1 ">
    <section>
      <h2 class="text-center mb-4">Cliente registrato</h2>
      <?php if(isset($errorelogin)): ?>
        <span class="alert alert-danger delay-alert p-0" role="alert"><?php echo $errorelogin; ?></span>
      <?php endif; ?>
      <form action="#" method="POST">
        <div class="form-row">
          <label for="email" class="form-label">Email address</label>
          <input type="email" class="form-control" name="email" id="email"/>
        </div>
        <div class="form-row mt-3">
          <label for="password" class="form-label">Password</label>
          <input type="password" class="form-control" name="password" id="password"/>
        </div>
        <button type="submit" class="btn btn-success mt-4">Accedi</button>
      </form>
    </section>
  </div>
  <div class="col-12 col-md-5 offset-md-1">
    <section>
      <h2 class="text-center mb-4">Nuovo cliente</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <div class="text-right">
        <a class="btn btn-success mt-4" href="registrazione.php">Crea account</a>
      </div>
    </section>
  </div>
</div>
