<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <title><?php echo $templateParams["titolo"]; ?></title>
  </head>
  <body>
    <div class="container-fluid">
      <header>
        <div class="row">
            <div class="col-4 offset-4 offset-md-1 col-md-2 p-md-4">
              <img class="img-fluid" src="<?php echo IMG_ICONE_DIR."logo.png"; ?>" alt=""/>
            </div>
            <div class="container-fluid align-self-center col-10 offset-1 offset-md-0 col-md-5 mx-md-5 my-3">
              <div class="row">
                <h1 class="text-center mb-4"><?php echo $datiAzienda["nomeSito"] ?></h1>
              </div>
              <form class="d-flex" action="./ricerca.php" method="get">
                <label for="search" class="visually-hidden">Cerca Prodotto</label>
                <input class="form-control me-2" type="search" id="search" name="nomeProdotto" placeholder="Cerca prodotto..." aria-label="Search"/>
                <button class="btn btn-outline-success" type="submit"><span class="fa fa-search"></span></button>
              </form>
            </div>
            <div class="col-2 col-md-1 offset-4 offset-md-0 d-flex align-self-end mb-3 mb-md-5">
              <div class="position-relative">
                <span class="badge position-absolute top-0 start-100 bg-danger translate-middle" id="badge-notifiche"><?php if(isset($templateParams["numNuoveNotifiche"])){echo $templateParams["numNuoveNotifiche"];}?></span>
                <a href="notifiche.php" class="p-0 p-xl-4 btn <?php if(!isUserLoggedIn()){ echo "disabled";} ?>" <?php if(!isUserLoggedIn()){ echo "aria-disabled=true";} ?>><img class="img-fluid" alt="notifica" src="<?php echo IMG_ICONE_DIR."notifica.png"; ?>"/></a>
              </div>
            </div>
            <div class="col-2 col-md-1 d-flex align-self-end mb-3 mb-md-5">
              <div class="position-relative">
                  <span class="badge position-absolute top-0 start-100 bg-danger translate-middle" id="badge-carrello"><?php if(getNumProdottiCarrello()>0) { echo getNumProdottiCarrello(); } ?></span>
                <a href="carrello.php" class="p-0 p-xl-4 btn <?php if(isUserLoggedIn() && isUserAdmin()){ echo "disabled";}?>" <?php  if(isUserLoggedIn() && isUserAdmin()){ echo "aria-disabled=true";} ?>><img class="img-fluid" alt="carrello" src="<?php echo IMG_ICONE_DIR."carrello.png";?>"/></a>
              </div>
            </div>
        </div>
      </header>
      <div class="row col-md-10 offset-md-1">
        <nav class="navbar navbar-expand-md navbar-light bg-light fw-bold">
          <div class="container-fluid">
            <a class="navbar-brand d-md-none text-success" href="#">Menu</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item offset-md-1">
                  <a class="nav-link text-success" aria-current="page" href="./index.php">Home</a>
                </li>
                <li class="nav-item dropdown offset-md-1">
                  <a class="nav-link dropdown-toggle text-success" id="dropdownCategorie" role="button" data-bs-toggle="dropdown" aria-expanded="false">Categorie</a>
                  <ul class="dropdown-menu offset-md-1" aria-labelledby="dropdownCategorie">
                    <?php foreach ($templateParams["categorie"] as $categoria): ?>
                      <li><a class="dropdown-item" href="./categoria.php?idCategoria=<?php echo $categoria["id"]; ?>"><?php echo $categoria["nome"]; ?></a></li>
                    <?php endforeach; ?>
                  </ul>
                </li>
                <li class="nav-item offset-md-1">
                  <a class="nav-link text-success" href="./contatti.php">Contatti</a>
                </li>
                <?php if (!isUserLoggedIn()): ?>
                  <li class="nav-item offset-md-1">
                    <a class="nav-link text-success" href="./login.php">Login</a>
                  </li>
                <?php else: ?>
                  <li class="nav-item dropdown offset-md-1">
                    <a class="nav-link dropdown-toggle text-success" id="dropdownAccount" role="button" data-bs-toggle="dropdown" aria-expanded="false">Account</a>
                    <ul class="dropdown-menu offset-md-1" aria-labelledby="dropdownAccount">
                      <li><a class="dropdown-item" href="./datiUtente.php">I miei dati</a></li>
                      <?php  if(isUserLoggedIn() && $_SESSION["ruolo"] === "venditore") :?>
                      <li><a class="dropdown-item" href="./prodotto.php?action=3">Aggiungi prodotto</a></li>
                      <li><a class="dropdown-item" href="./ordiniAdmin.php">Ordini</a></li>
                      <?php else: ?>
                      <li><a class="dropdown-item" href="./ordini.php">I miei ordini</a></li>
                      <?php endif;  ?>
                      <li><a class="dropdown-item" href="./logout.php">Logout</a></li>
                    </ul>
                  </li>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div class="row">
      <?php
        if(isset($templateParams["mainTemplate"])){
            require($templateParams["mainTemplate"]);
        }
      ?>
      <?php
        if(isset($templateParams["asideTemplate"])) {
            require($templateParams["asideTemplate"]);
        }
      ?>
      </div>
      <footer class="page-footer font-small text-white bg-dark mt-5">
        <div class="container">
          <div class="row">
            <div class="col-md-3 offset-md-1 text-center">
              <h3 class="text-uppercase mb-4 font-weight-bold mt-5">Dove siamo</h3>
                <hr class="mb-4 mt-0 d-inline-block mx-auto col-5">
                  <ul class="list-unstyled">
                    <li class="location"> <span class="col-1 fa fa-map-marker"></span><?php echo $datiAzienda["indirizzo"];?> </li>
                    <li class=""><?php echo $datiAzienda["CAP"].", ".$datiAzienda["citta"]?> </li>
                    <li class=""><?php echo $datiAzienda["provincia"]?> </li>
                  </ul>
            </div>
            <div class="col-8 col-md-3 offset-3 offset-md-1">
              <h3 class="text-uppercase mb-4 font-weight-bold mt-5">Contatti</h3>
              <hr class="mb-4 mt-0 d-inline-block mx-auto col-5">
              <ul class="list-unstyled">
                <li class="location"> <span class="col-1 fa fa-phone"></span><?php echo $datiAzienda["telefono"];?> </li>
                <li class="location"> <span class="col-1 fa fa-envelope"></span><?php echo $datiAzienda["email"];?> </li>
              </ul>
            </div>
            <div class="col-8 col-md-3 offset-3 offset-md-1">
              <div class="row">
                <h3 class="text-uppercase mb-4 font-weight-bold mt-5">Seguici su:</h3>
                <hr class="mb-4 mt-0 d-inline-block col-5 ms-2">
              </div>
              <div class="row">
                <div class="col">
                  <?php if(isset($datiAzienda["twitter"])):?>
                  <a href="<?php echo $datiAzienda["twitter"] ?>" class="social" aria-label="Twitter"><span class="fa fa-twitter"></span></a>
                  <?php endif ?>
                  <?php if(isset($datiAzienda["facebook"])):?>
                  <a href="<?php echo $datiAzienda["facebook"] ?>" class="ms-4 social" aria-label="Facebook"><span class="fa fa-facebook"></span></a>
                  <?php endif ?>
                  <?php if (isset($datiAzienda["instagram"])):?>
                  <a href="<?php echo $datiAzienda["instagram"]?>" class="ms-4 social" aria-label="Instagram"><span class="fa fa-instagram"></span></a>
                  <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-copyright text-white bg-dark text-center py-3 mt-5">
          <p>© 2020 Copyright</p>
        </div>
      </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <?php
      if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
      endif;
    ?>
  </body>
</html>
