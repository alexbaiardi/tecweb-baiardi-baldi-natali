<main>
 <div class="row col-md-10 offset-md-1 mt-3">
   <section>
     <div class="text-center">
       <h2>Notifiche</h2>
     </div>
     <?php if (count($templateParams["notifiche"])==0): ?>
     <div class="py-4 ">
       <span class="alert alert-danger" role="alert">
         Non sono presenti notifiche!
       </span>
     </div>
    <?php else: ?>
    <div class="row mt-3">
    <?php foreach ($templateParams["notifiche"] as $notifica): ?>
      <div class="card m-2">
        <div class="card-body">
          <div class="notifica-intestazione">
            <?php if (!$notifica["visualizzata"]): ?>
              <span class="badge position-absolute top-0 end-0 bg-primary d-flex badge-notifica m-2">Nuova Notifica</span>
            <?php endif;?>
            <div class="row d-flex justify-content-around align-items-center">
              <h3 class="fs-5 mt-2 col-6 col-md-4"><?php echo $notifica["nome"] ?></h3>
              <p class="mt-3 col-6 col-md-3"><?php echo date("d-m-Y H:i",strtotime($notifica["data"])); ?></p>
              <form class="form visualizza col" action="visualizzaNotifica.php" method="post">
                <input type="hidden" name="idNotifica" value="<?php echo $notifica["id"];?>"/>
                <button type="<?php echo ($notifica["visualizzata"]) ? "button" : "submit" ; ?>" class="btn btn-success visualizza-notifica">Visualizza notifica</button>
              </form>
              <form class="form elimina col-2" action="eliminaNotifica.php" method="post">
                <input type="hidden" name="idNotifica" value="<?php echo $notifica["id"];?>"/>
                <button type="submit" class="btn-close <?php if(!$notifica["visualizzata"]){ echo "d-none";}?>" aria-label="Close"></button>
              </form>
            </div>
          </div>
          <div class=" notifica-dettaglio">
            <div class="d-flex justify-content-around">
                <p class="col-8"><?php echo $notifica["descrizione"];?></p>
                <?php if (isset($notifica["idOrdine"])): ?>
                  <p class="col-4">Ordine: <?php echo $notifica["idOrdine"];?></p>
                <?php elseif (isset($notifica["idProdotto"])): ?>
                  <p class="col-4">Prodotto: <?php echo $dbProdotti->getProdottoById($notifica["idProdotto"])["nome"];?></p>
                <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
     <?php endforeach;
     endif; ?>
      </div>
    </section>
  </div>
</main>
