<div class="card mb-4 shadow-sm d-flex flex-fill">
  <a href="./prodotto.php?id=<?php echo $prodotto["id"];  ?>">
    <div class="ratio ratio-4x3 position-relative">
      <img class="img-fluid img-fit-product" src="<?php echo IMG_PRODOTTI_DIR.$prodotto["immagine"]; ?>" alt=""/>
    </div>
  </a>
  <?php if ($prodotto["patentino"]): ?>
    <span class="badge position-absolute top-0 bg-danger d-flex col-4 m-2">Patentino</span>
  <?php endif;?>
  <div class="card-body mt-3">
    <a href="./prodotto.php?id=<?php echo $prodotto["id"];  ?>" class="text-reset text-decoration-none"><h3 class="card-title fs-4"> <?php echo $prodotto["nome"]; ?></h3></a>
    <strong>Prezzo : <?php echo $prodotto["prezzo"]; ?> €</strong>
    <p> Quantità disponibile : <?php echo $prodotto["quantitaDisponibile"]; ?></p>
    <div class="row">
      <div class="col text-center mt-3">
        <a href="./prodotto.php?id=<?php echo $prodotto["id"];  ?>" class="btn btn-success"> Modifica</a>
      </div>
      <div class="col text-center mt-3">
        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalCancelProduct" data-bs-whatever="<?php echo $prodotto["id"];  ?>">Cancella</button>
      </div>
    </div>
  </div>
</div>
