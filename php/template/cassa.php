<div class="row">
  <header>
    <div class="text-center mt-3">
      <h2>Dati fatturazione</h2>
    </div>
  </header>
  <div class="col-md-10 offset-md-1 mt-5">
    <h3 class="mb-3 text-center">Indirizzo di spedizione</h4>
    <?php if(isset($erroreIndirizzo)): ?>
    <span class="alert alert-danger delay-alert p-0" role="alert"> <?php echo $erroreIndirizzo; ?> </span>
    <?php endif; ?>
    <form class="form" action="./confermaOrdine.php" method="POST">
      <div class="row">
        <div class="col-md-9 mb-3">
          <label for="indirizzo" class="form-label">Indirizzo *</label>
          <input type="text" class="form-control" name="indirizzo" id="indirizzo" value="<?php echo $datiUtente["indirizzo"] ?>" required minlength="2"/>
        </div>
        <div class="col-md-3 mb-3">
          <label for="CAP" class="form-label">CAP *</label>
          <input type="text" class="form-control" name="CAP" id="CAP" value="<?php echo $datiUtente["CAP"] ?>" required minlength="5" maxlength="5"/>
        </div>
      </div>
      <hr class="my-4">
      <div class="form-check">
        <input type="checkbox" class="form-check-input" name="salva" id="salva"/>
        <label class="form-check-label" for="salva">Salva per i prossimi ordini</label>
      </div>
      <hr class="my-4">
      <h3 class="mb-3 text-center">Pagamento</h3>
      <?php if(isset($errorePagamento)): ?>
      <span class="alert alert-danger delay-alert p-0" role="alert"> <?php echo $errorePagamento; ?> </span>
      <?php endif; ?>
      <fieldset>
        <legend class="fs-6">Metodo di pagamento</legend>
        <div class="my-3">
          <div class="form-check">
            <input id="carta" name="metodoPagamento" type="radio" class="form-check-input"/>
            <label class="form-check-label" for="carta">Carta di credito</label>
          </div>
          <div class="form-check">
            <input id="contrassegno" name="metodoPagamento" type="radio" class="form-check-input"/>
            <label class="form-check-label" for="contrassegno">Contrassegno</label>
          </div>
          <input type="hidden" name="metodo" value="carta" id="metodo"/>
        </div>
      </fieldset>

      <div class="row" id="pagamento">
        <div class="col-md-6 mb-3">
        <label for="titolare" class="form-label">Titolare</label>
        <input type="text" class="form-control" id="titolare" minlength="2"/>
        </div>

        <div class="col-md-6 mb-3">
          <label for="numeroCarta" class="form-label">Numero carta</label>
          <input type="text" class="form-control" id="numeroCarta" name="numeroCarta" minlength="13" maxlength="16"/>
        </div>

        <div class="col-md-3 mb-3">
          <label for="scadenza" class="form-label">Valida fino a </label>
          <input type="month" class="form-control" name="scadenza" id="scadenza"/>
        </div>

        <div class="col-md-3 mb-3">
          <label for="cvv" class="form-label">CVV</label>
          <input type="text" class="form-control" name="cvv" id="cvv" minlength="3" maxlength="3"/>
        </div>
      </div>
      <hr class="my-4">
      <button class="col-12 col-md-6 offset-md-6  btn btn-success btn-lg" type="submit">Conferma ordine</button>
    </form>
  </div>
</div>
