<div class="row mt-md-5">
  <div class="col-12 col-md-10 offset-1">
    <h2 class="text-center mt-2 mb-4">I miei dati</h2>
    <form class="row" action="#" method="POST" enctype="multipart/form-data">
      <div class="col-12 col-md-6 offset-md-3">
        <label for="nome" class="form-label">Nome</label>
        <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $datiUtente["nome"] ?>" readonly/>
      </div>
      <div class="col-12 col-md-6 offset-md-3">
        <label for="cognome" class="form-label">Cognome</label>
        <input type="text" class="form-control" id="cognome" name="cognome" value="<?php echo $datiUtente["cognome"] ?>" readonly/>
      </div>
      <div class="col-12 col-md-6 offset-md-3">
        <label for="email" class="form-label">Email address</label>
        <input type="email" class="form-control" name="email" value="<?php echo $datiUtente["email"] ?>" readonly id="email"/>
      </div>
      <?php if ($datiUtente["patentino"]):?>
        <div class="col-12 col-md-6 offset-md-3">
          <label for="idPat" class="form-label">Numero patentino</label>
          <input type="text" class="form-control" name="idPatentino" id="idPat" minlength="5" value="<?php echo $datiUtente["idPatentino"] ?>" readonly/>
        </div>
      <?php endif; ?>
      <input type="hidden" name="modifica" value="si"/>
      <button type="submit" class="btn btn-success mt-4 col-12 px-0 col-md-6 offset-md-3">Modifica</button>
    </form>
  </div>
</div>
