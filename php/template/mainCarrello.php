<main>
 <div class="row col-md-10 offset-md-1">
   <div class="col-lg-8 p-0 bg-light">
     <div class="m-3 bg-white">
       <section>
         <h2>Carrello</h2>
         <?php if (count($templateParams["carrello"])==0): ?>
         <div class="py-4 ">
           <span class="alert alert-danger" role="alert">
             Non sono presenti prodotti nel carrello!
           </span>
         </div>
         <?php else: ?>
         <?php foreach ($templateParams["carrello"] as $prodotto): ?>
           <div class="row py-3 riga-carrello">
             <div class="col-sm-3 position-relative">
               <div class="ratio ratio-4x3">
                 <img class="img-fluid img-fit-product mt-2" src="<?php echo IMG_PRODOTTI_DIR.$prodotto["immagine"] ?>" alt="" />
               </div>
               <?php
                if ($prodotto["patentino"]) {
                  echo "<span class=\"badge position-absolute top-0 end-0 bg-danger d-flex col-5 m-4\">Patentino</span>";
                }
               ?>
             </div>
             <div class="col-sm-9">
               <h3 class="mt-3"><?php echo $prodotto["nome"] ?></h3>
               <strong>Prezzo: <?php echo $prodotto["prezzo"] ?> €</strong>
               <div class="d-flex flex-row align-items-center">
                 <div class="col-8 mt-2 d-flex">
                   <form class="form col-12 col-xl-9" action="modificaCarrello.php" method="post">
                     <input type="hidden" name="idProdotto" value="<?php echo $prodotto["id"]?>"/>
                     <label for="quantita_<?php echo $prodotto["id"] ?>" class="form-label col-3 offset-1 mt-3 ms-0 me-sm-3">Quantità:</label>
                     <div class="col-7 col-sm-6 float-end me-sm-3 me-1">
                       <div class="input-group my-2">
                         <button type="button" class="diminuisci modifica-quantita btn btn-success btn-number">-</button>
                         <input type="number" id="quantita_<?php echo $prodotto["id"] ?>" name="quantita" class="form-control input-number" min="1" max="<?php echo $prodotto["quantitaDisponibile"]; ?>" value="<?php echo $prodotto["quantita"]; ?>"/>
                         <button type="button" class="aumenta modifica-quantita btn btn-success btn-number">+</button>
                       </div>
                     </div>
                   </form>
                 </div>
                 <div class="col-4 mt-2 d-flex">
                   <form class="form rimuovi" action="modificaCarrello.php" method="post">
                     <input type="hidden" name="idProdotto" value="<?php echo $prodotto["id"]?>"/>
                     <button type="submit" class="btn btn-success"><span class="fa fa-trash me-2"></span>Rimuovi</a>
                   </form>
                 </div>
               </div>
             </div>
           </div>
         <?php endforeach;
         endif; ?>
        </section>
     </div>
    </div>
    <div class="col-lg-4 p-0 bg-light">
      <div class="m-3 ms-0 bg-white p-4">
        <aside>
          <h3>Totale</h3>
          <div class="mt-4">
            <span class="fw-bold fs-5 me-3">Totale (IVA inclusa)</span>
            <strong class="fs-4 d-flex" id="subtotale"> <?php echo $templateParams["subtotale"]; ?> €</strong>
          </div>
          <div class="text-center p-4">
            <a href="./cassa.php" class="btn btn-success px-4">Vai alla cassa</a>
          </div>
        </aside>
      </div>
    </div>
  </div>
</main>
