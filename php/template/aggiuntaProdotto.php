<div class="col-md-9 col-lg-7 order-md-2 d-flex align-self-stretch mt-4">
  <main class="col">
    <section>
      <header>
        <h2 class="text-center">Aggiungi Nuovo Prodotto</h2>
      </header>
      <?php if(isset($templateParams["messaggio"] )): ?>
        <?php if($templateParams["messaggio"]["tipo"] == "Errori"): ?>
        <div class="mt-2">
          <p><span class="fa fa-exclamation-triangle text-danger me-2"></span><?php echo $templateParams["messaggio"]["tipo"].":" ?></p>
          <ul>
            <?php foreach ($templateParams["messaggio"]["testo"] as $info): ?>
              <li><?php echo $info ?></li>
            <?php endforeach; ?>
          </ul>
        </div>
        <?php endif ?>
      <?php endif ?>
      <form  action="./operazioniProdotto.php?action=3" method="POST" enctype="multipart/form-data" class="mt-5">
        <div class="row">
          <div class="col">
            <label for="immagineProdotto" class="form-label">Inserisci immagine Prodotto</label>
            <input class="form-control" type="file" name="immagineProdotto" id="immagineProdotto" required/>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-6 mt-3">
            <label for="nomeProdotto" class="form-label">Nome prodotto</label>
            <input class="form-control" type="text" name="nomeProdotto" id="nomeProdotto" required />
          </div>
          <div class="col-12 col-md-6 mt-3">
            <label for="quantitaProdotto" class="form-label">Quantita disponibile</label>
            <input class="form-control" type="number" name="quantitaProdotto" id="quantitaProdotto" required/>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-6 mt-3">
            <label for="formMarca" class="form-label">Marca</label>
            <input class="form-control" list="datalistOptions" type="text" name="marca" id="formMarca" required/>
            <datalist id="datalistOptions">
              <?php foreach ($templateParams["marche"] as $marcaSelezionata): ?>
                <option value="<?php echo $marcaSelezionata["nome"] ?>"></option>
              <?php endforeach; ?>
            </datalist>
          </div>
          <div class="col-12 col-md-6 mt-3">
            <label for="categoria" class="form-label">Categoria</label>
            <select class="form-select" name="categoria"  id="categoria" required />
              <option value="" selected disabled>Seleziona categoria</option>
              <?php foreach ($templateParams["categorie"] as $categoria): ?>
                <option value="<?php echo $categoria["id"] ?>"><?php echo $categoria["nome"] ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-md-6 mt-3">
            <label for="prezzo" class="form-label">Prezzo</label>
            <input class="form-control" type="number" step="any" name="prezzoProdotto" id="prezzo" required/>
          </div>
          <div class="col-12 col-md-6 mt-3">
            <label for="sitoWeb" class="form-label">Sito Web</label><small class="text-muted ms-2">(Opzionale)</small>
            <input class="form-control" type="url" name="sitoWeb" id="sitoWeb" />
          </div>
        </div>
        <div class="mt-3">
          <fieldset>
            <legend class="col-3 fs-6">Patentino:</legend>
            <div class="form-check form-check-inline col-3">
              <input class="form-check-input" type="radio" name="radioPat" id="radioPatSi" value="1" />
              <label class="form-check-label" for="radioPatSi">Si</label>
            </div>
            <div class="form-check form-check-inline col-3">
              <input class="form-check-input" type="radio" name="radioPat" value="0" id="radioPatNo" checked />
              <label class="form-check-label" for="radioPatNo">No</label>
            </div>
          </fieldset>
        </div>
        <div class="col-md-8 offset-md-2 mt-4 text-center">
          <label class="col-12 text-center" for="descrizione">Descrizione prodotto</label>
          <textarea name="descrizione" class="form-control" id="descrizione" required></textarea>
        </div>
        <div class="row mt-5">
          <div class="col text-center">
            <button type="submit" class="checkInput btn btn-success" name="aggiuntaProdotto">Aggiungi Prodotto</button>
          </div>
        </div>
      </form>
    </section>
  </main>
</div>
