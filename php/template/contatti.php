<div>
  <main>
    <section>
      <header>
        <h2 class="text-center mt-5">Chi siamo</h2>
      </header>
      <div class="row offset-md-1 mt-5">
        <div class="col-md-5 mt-4">
          <p><?php echo $datiAzienda["descrizione"];?></p>
        </div>
        <div class="col-md-5">
          <div class="ratio ratio-4x3 mt-3">
            <img class="img-fluid img-fit" src="./img/<?php echo $datiAzienda["immagine"];?>" alt=""/>
          </div>
        </div>
      </div>
        <div class="text-center mt-5">
          <h3>Contatti</h3>
          <p>
            La nosta sede si trova in <?php echo $datiAzienda["indirizzo"];?> a <?php echo $datiAzienda["citta"];?>, <?php echo $datiAzienda["CAP"];?> (<?php echo $datiAzienda["provincia"];?>)
          </p>
          <p>
            Per qualsiasi informazione non esitare a contattarci! Puoi venire a trovarci anche presso la nostra azienda. Ti aspettiamo!
          </p>
          <p>
             Telefono : <?php echo $datiAzienda["telefono"];?><br/>
             Indirizzo e-mail : <?php echo $datiAzienda["email"];?><br/>
             Ragione sociale : <?php echo $datiAzienda["ragioneSociale"];?>
          </p>
        </div>
    </section>
  </main>
</div>
