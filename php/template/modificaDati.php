<div class="row mt-md-5">
  <div class="col-12 col-md-10 offset-md-1">
    <h2 class="text-center mt-2 mb-4">Aggiorna i tuoi dati</h2>
    <?php if(isset($erroreAggiornamento)): ?>
    <p><?php echo $erroreAggiornamento; ?></p>
    <?php endif; ?>
    <form class="row" action="#" method="POST" enctype="multipart/form-data" id="modificaDati.php">
      <div class="col-12 col-md-6 mt-3">
        <label for="email" class="form-label">Nuova email</label>
        <input type="email" class="form-control" name="email" value=<?php echo $datiUtente["email"] ?> id="email"/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <label for="emailConf" class="form-label">Conferma nuova email</label>
        <input type="email" class="form-control" value="<?php echo $datiUtente["email"]?>" id="emailConf"/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <label for="password" class="form-label">Nuova password</label>
        <input type="password" class="form-control" name="password" id="password"/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <label for="passwordConf" class="form-label">Conferma nuova password</label>
        <input type="password" class="form-control" id="passwordConf"/>
      </div>
      <div class="col-12 col-md-6 mt-3">
        <?php if ($datiUtente["patentino"]): ?>
          <label for="modificaPat" class="custom-control-label">Modifica il tuo patentino</label>
          <input class="custom-control-input" type="checkbox" id="modificaPat"/>
        <?php elseif(!isUserAdmin()): ?>
          <label for="modificaPat" class="custom-control-label">Aggiungi patentino</label>
          <input class="custom-control-input" type="checkbox" id="modificaPat"/>
        <?php endif; ?>
      </div>
      <div class="row"></div>
      <div class="col-12 col-md-6 mt-3" id="numPat">
        <label for="idPat" class="form-label">Numero patentino</label>
        <input type="text" class="form-control" name="idPatentino"
        value="<?php if ($datiUtente["patentino"]) {
          echo $datiUtente["idPatentino"];
        } ?>" id="idPat" minlength="5"/>
      </div>
      <div class="col-12 col-md-6 mt-3" id="uploadPat">
        <label for="pat" class="form-label">Carica patentino</label>
        <input class="form-control" type="file" name="imgPatentino" id="pat"/>
      </div>
      <input type="hidden" name="modifica" value="no"/>
      <button type="submit" class="btn btn-success mt-4 col-12 px-0 col-md-4 offset-md-8">Salva le modifiche</button>
    </form>
  </div>
</div>
