<div class="card mb-4 shadow-sm d-flex flex-fill">
  <a href="./prodotto.php?id=<?php echo $prodotto["id"];  ?>">
    <div class="ratio ratio-4x3 position-relative">
      <img class="img-fluid img-fit-product" src="<?php echo IMG_PRODOTTI_DIR.$prodotto["immagine"]; ?>" alt=""/>
    </div>
  </a>
  <?php if ($prodotto["patentino"]): ?>
    <span class="badge position-absolute top-0 bg-danger d-flex col-4 m-2">Patentino</span>
  <?php endif;?>
  <div class="card-body mt-3">
    <a href="./prodotto.php?id=<?php echo $prodotto["id"];  ?>" class="text-reset text-decoration-none"><h3 class="card-title fs-4"> <?php echo $prodotto["nome"]; ?></h3></a>
    <strong>Prezzo : <?php echo $prodotto["prezzo"]; ?> €</strong>
    <?php  if($prodotto["quantitaDisponibile"] != 0) : ?>
      <form class="form carrello" action="aggiungiAlCarrello.php" method="post">
      <input type="hidden" name="idProdotto" value="<?php echo $prodotto["id"]?>"/>
        <label for="quantita_<?php echo $prodotto["id"]?>" class="form-label col-5 mt-3">Quantità:</label>
        <div class="col-7 float-end mt-2">
          <div class="input-group">
            <button type="button" class="diminuisci btn btn-success btn-number">-</button>
            <input type="number" id="quantita_<?php echo $prodotto["id"]?>" name="quantita" class="form-control input-number" min="1" max="<?php echo $prodotto["quantitaDisponibile"]; ?>" value="1"/>
            <button type="button" class="aumenta btn btn-success btn-number">+</button>
          </div>
        </div>
        <div class="info mt-2">
          <p><span class="fa fa-check-circle text-success me-2"></span>Prodotto disponibile</p>
        </div>
        <div class="text-center">
          <button type="submit" class="btn btn-success text-align-center align-items-center mt-4"><span class="fa fa-shopping-cart me-2"></span>Aggiungi al carrello</a>
        </div>
      </form>
      <?php else: ?>
        <div class="mt-2">
            <p><span class="fa fa-times-circle text-danger me-2"></span>Prodotto non disponibile</p>
        </div>
      <?php endif ?>
  </div>
</div>
