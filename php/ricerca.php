<?php
  require_once("setup.php");

  $templateParams["titolo"]="Fitonline - Risultati ricerca";
  $templateParams["mainTemplate"]="paginaRicerca.php";
  $templateParams["asideTemplate"]="asideCatalogo.php";
  $templateParams["marche"]=$dbh->getMarche();

  if(isUserLoggedIn() && isUserAdmin()){
      $templateParams["js"]=array("js/modalOption.js");
  }else {
      $templateParams["js"]=array("js/quantita.js","js/aggiungiCarrello.js");
  }

  define("MAX_ELEMENTI_PAGINA", 9);
  if(isset($_GET["pagina"]) && is_numeric($_GET["pagina"])){
    $pagina=$_GET["pagina"];
  } else {
    $pagina=1;
  }

  if(isset($_REQUEST["marca"]) && is_numeric($_REQUEST["marca"])){
      $idMarca = $_REQUEST["marca"];
      if(isset($_REQUEST["nomeProdotto"])){
          $nomeProdotto = $_REQUEST["nomeProdotto"];
          $numProdotti=$dbProdotti->getNumProdottiByMarcaNome($idMarca,$nomeProdotto);
          list($paginaCorrente,$offset,$pagineTotali)=getPageOffsetTotalPage($pagina,MAX_ELEMENTI_PAGINA,$numProdotti);
          $templateParams["prodotti"]=$dbProdotti->getProdottiByMarcaNome($idMarca,$nomeProdotto,MAX_ELEMENTI_PAGINA,$offset);
          $templateParams["parametriRicerca"]="marca=".$idMarca."&nomeProdotto=".$nomeProdotto;
      } else {
        $numProdotti=$dbProdotti->getNumProdottiByMarca($idMarca);
        list($paginaCorrente,$offset,$pagineTotali)=getPageOffsetTotalPage($pagina,MAX_ELEMENTI_PAGINA,$numProdotti);
        $templateParams["prodotti"]=$dbProdotti->getProdottiByMarca($idMarca,MAX_ELEMENTI_PAGINA,$offset);
        $templateParams["parametriRicerca"]="marca=".$idMarca;
      }
  } else {
    if(isset($_REQUEST["nomeProdotto"]) && $_REQUEST["nomeProdotto"]!=null){
        $nomeProdotto = $_REQUEST["nomeProdotto"];
        $numProdotti=$dbProdotti->getNumProdottiByNome($nomeProdotto);
        list($paginaCorrente,$offset,$pagineTotali)=getPageOffsetTotalPage($pagina,MAX_ELEMENTI_PAGINA,$numProdotti);
        $templateParams["prodotti"]=$dbProdotti->getProdottiByNome($nomeProdotto,MAX_ELEMENTI_PAGINA,$offset);
        $templateParams["parametriRicerca"]="nomeProdotto=".$nomeProdotto;
    } else {
        header("location:javascript://history.go(-1)");
    }
  }

  $templateParams["numRisultati"]=$numProdotti;
  $templateParams["paginaCorrente"]=$paginaCorrente;
  $templateParams["pagineTotali"]=$pagineTotali;

  require 'template/base.php';
 ?>
