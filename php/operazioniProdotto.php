<?php
  require_once("setup.php");
  if(!isset($_REQUEST["action"]) || !(isUserLoggedIn() && isUserAdmin()) || !in_array($_REQUEST["action"],array(1,2,3)) ){
    header('location: index.php');
  }

  if($_REQUEST["action"]==1){
      $dbProdotti->cancellaProdotto($_GET["id"]);
      header("location: index.php");
  }

  if($_REQUEST["action"]==2){
    $testo = array();
    $error = false;
    if(isset($_POST["nomeProdotto"]) &&isset($_POST["quantitaProdotto"]) && isset($_POST["prezzoProdotto"]) &&isset($_POST["radioPat"]) && isset($_POST["desc"])){
      $idProd = $_POST["idProd"];
      $nomeProdotto = $_POST["nomeProdotto"];
      $quantitaProdotto = $_POST["quantitaProdotto"];
      $patentino = $_POST["radioPat"];
      $descrizione = $_POST["desc"];
      $prezzo = $_POST["prezzoProdotto"];
      $prodotto=$dbProdotti->getProdottoById($idProd);
      if(isset($_POST["sitoWeb"]) && !empty($_POST["sitoWeb"]) ) {
        var_dump($_POST["sitoWeb"]);
        $sito = $_POST["sitoWeb"];
        $sito = filter_var($sito, FILTER_SANITIZE_URL);
        if(!filter_var($sito, FILTER_VALIDATE_URL)){
          $error = true;
          array_push($testo,"sito web non valido");
          $sito = $prodotto["sitoWeb"];
        }
      }else {
        $sito = $prodotto["sitoWeb"];
      }
    }else {
      $error = true;
      array_push($testo,"Errore nell'inserimento dei dati");
    }
    if($_FILES['immagineProdotto']['size'] > 0 && $_FILES["immagineProdotto"]["error"] == 0){
      list($result, $msg) = uploadImage(IMG_PRODOTTI_DIR, $_FILES["immagineProdotto"]);
      if($result==1){
          unlink(IMG_PRODOTTI_DIR.$prodotto["immagine"]);
          $immagineProdotto = $msg;
        }else{
          $error = true;
          array_push($testo,$msg);
          $immagineProdotto = $prodotto["immagine"];
        }
    }else {
        $immagineProdotto = $prodotto["immagine"];
    }
    $dbProdotti->modificaProdotto($idProd,$nomeProdotto,$descrizione,$prezzo,$quantitaProdotto,$sito,$immagineProdotto,$patentino);
    if($error== true){
      $templateParams["messaggio"] =array('tipo' => "Errori" ,'testo' => $testo);
    }else {
      $templateParams["messaggio"] =array('tipo' => "Successo" ,'testo' => "Modifica avvenuta con successo");
    }
    $checkOperazioneProd = $idProd;
  }

if($_REQUEST["action"]==3) {
  $erroreAggiunta= false;
  $error = false;
  $testo = array();
  if(isset($_POST["nomeProdotto"]) && isset($_POST["quantitaProdotto"]) &&isset($_POST["marca"]) &&isset($_POST["categoria"]) &&isset($_POST["prezzoProdotto"]) &&isset($_POST["radioPat"]) && isset($_POST["descrizione"])){
    $nome = $_POST["nomeProdotto"];
    $quantita = $_POST["quantitaProdotto"];
    $marcaProdotto = $_POST["marca"];
    $categoria = $_POST["categoria"];
    $prezzo = $_POST["prezzoProdotto"];
    $patentino = $_POST["radioPat"];
    $descrizione = $_POST["descrizione"];
    if(isset($_POST["sitoWeb"])) {
      $sito = $_POST["sitoWeb"];
      $sito = filter_var($sito, FILTER_SANITIZE_URL);
      if(!filter_var($sito, FILTER_VALIDATE_URL)){
          $sito= NULL;
      }
    }else {
      $sito=NULL;
    }
  }else {
    $error = true;
    array_push($testo,"Errore inserimento dati");
  }
  if($_FILES['immagineProdotto']['size'] > 0 && $_FILES["immagineProdotto"]["error"] == 0){
    list($result, $msg) = uploadImage(IMG_PRODOTTI_DIR, $_FILES["immagineProdotto"]);
    if($result==1){
      $immagineProdotto = $msg;
    }else {
      $error = true;
      array_push($testo,$msg);
    }
  }else {
    $error = true;
    array_push($testo,"Immagine obbligatoria");
  }
  $marche = $dbh->getMarche();
  $newMarca = true;
  foreach ($marche as $marca) {
    if($marca["nome"] == $marcaProdotto){
      $newMarca = false;
      $idmarca = $marca["id"];
      break;
    }
  }
  if($newMarca == true){
    $idmarca = $dbh->aggiungiMarca($marcaProdotto);
  }
  if($error== true){
    $erroreAggiunta = true;
    $templateParams["messaggio"] =array('tipo' => "Errori" ,"testo" =>$testo);
    }else{
      $idProd= $dbProdotti->aggiungiProdotto($nome,$descrizione,$prezzo,$quantita,$categoria,$idmarca,$sito,$immagineProdotto,$patentino,0);
      $templateParams["messaggio"] =array('tipo' => "Successo" ,'testo' => "Aggiunta del prodotto eseguita!");
      $checkOperazioneProd = $idProd;
    }
  }
  require 'prodotto.php';
 ?>
