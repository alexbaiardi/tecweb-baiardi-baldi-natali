<?php
  require_once("setup.php");
  $templateParams["titolo"]="Fitonline - Home";
  $templateParams["js"]=array("js/quantita.js","js/aggiungiCarrello.js","js/modalOption.js");
  $templateParams["mainTemplate"]="home.php";
  $templateParams["asideTemplate"]="asideHome.php";
  $templateParams["prodotti"]=$dbProdotti->getProdottiPiuVenduti();
  $templateParams["marche"]=$dbh->getMarche();
  require 'template/base.php';
 ?>
