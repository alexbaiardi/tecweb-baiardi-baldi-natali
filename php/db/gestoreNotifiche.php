<?php
require_once("databaseHelper.php");
function not_null($var){
  return !$var || $var!=null;
}
  class GestoreNotifiche extends DatabaseHelper{


    public function getNotificheByUtente($idUtente){
      $stmt = $this->db->prepare("SELECT tn.nome,tn.descrizione,nu.id,nu.visualizzata,nu.data,nu.idOrdine,nu.idProdotto
                                    FROM notifiche_utenti nu
                                      JOIN tipologie_notifiche tn
                                        ON nu.idNotifica=tn.id
                                    WHERE nu.idUtente=?
                                    ORDER BY nu.data DESC");
      $stmt->bind_param('i',$idUtente);
      $notifiche=$this->executeSelect($stmt);
      $tmpNotifiche=array();
      foreach ($notifiche as $notifica) {
        $notifica=array_filter($notifica, 'not_null');
        array_push($tmpNotifiche,$notifica);
      }
      return $tmpNotifiche;
    }

    public function getNumNuoveNotificheByUtente($idUtente){
      $stmt = $this->db->prepare("SELECT COUNT(*) as num
                                    FROM notifiche_utenti
                                    WHERE idUtente=? AND visualizzata=false");
      $stmt->bind_param('i',$idUtente);
      return $this->executeSelect($stmt)[0]["num"];
    }

    public function setNotificaVisualizzata($idNotifica){
      $stmt = $this->db->prepare("UPDATE notifiche_utenti
                                    SET visualizzata=true
                                    WHERE id=?");
      $stmt->bind_param('i',$idNotifica);
      $stmt->execute();
    }

    public function eliminaNotifica($idNotifica){
      $stmt = $this->db->prepare("DELETE
                                    FROM notifiche_utenti
                                    WHERE id=?");
      $stmt->bind_param('i',$idNotifica);
      $stmt->execute();
    }

    private function generaNotifica($idNotifica,$idUtente,$idOrdine,$idProdotto){
      $stmt = $this->db->prepare("INSERT INTO notifiche_utenti (idNotifica,idUtente,data,idOrdine,idProdotto)
                                    VALUES (?,?,NOW(),?,?)");
      $stmt->bind_param('iiii',$idNotifica,$idUtente,$idOrdine,$idProdotto);
      $stmt->execute();
    }

    public function generaNotificaConfermaOrdine($idUtente,$idOrdine){
      $this->generaNotifica(1,$idUtente,$idOrdine,null);
    }

    public function generaNotificaSpedizioneOrdine($idUtente,$idOrdine){
      $this->generaNotifica(2,$idUtente,$idOrdine,null);
    }

    public function generaNotificaNuovoOrdine($idUtente,$idOrdine){
      $this->generaNotifica(4,$idUtente,$idOrdine,null);
    }

    public function generaNotificaProdottoEsaurito($idUtente,$idProdotto){
      $this->generaNotifica(3,$idUtente,null,$idProdotto);
    }

  }
?>
