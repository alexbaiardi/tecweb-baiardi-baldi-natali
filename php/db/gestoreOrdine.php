<?php
require_once("databaseHelper.php");

  class GestoreOrdini extends DatabaseHelper{

    public function getOrdiniByUtente($id){
      $stmt = $this->db->prepare("SELECT o.id as id, data, SUM(quantita) as numArticoli,
                                (SELECT SUM(prezzo*quantita) FROM `righe_ordine` ro WHERE idOrdine = o.id) as totale, stato
                                FROM ordini o JOIN `righe_ordine`
                                ON o.id = idOrdine WHERE o.idCliente=?
                                GROUP BY o.id
                                ORDER BY o.id DESC");
      $stmt->bind_param('i',$id);
      return $this->executeSelect($stmt);
    }

    public function getDettaglioOrdine($id){
      $stmt = $this->db->prepare("SELECT nome,ro.prezzo,quantita
                                  FROM `righe_ordine` ro JOIN prodotti p
                                  ON ro.idProdotto = p.id
                                  WHERE ro.idOrdine = ?");
      $stmt->bind_param('i',$id);
      return $this->executeSelect($stmt);
    }

    public function inserisciOrdine($idCliente,$data,$stato)
    {
      $stmt = $this->db->prepare("INSERT INTO ordini (idCliente,data,stato)
                                  VALUES (?,?,?)");
      $stmt->bind_param('iss',$idCliente,$data,$stato);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function inserisciRigaOrdine($idOrdine,$idProdotto,$quantita,$prezzo)
    {
      $stmt = $this->db->prepare("INSERT INTO righe_ordine (idOrdine,idProdotto,quantita,prezzo)
                                  VALUES (?,?,?,?)");
      $stmt->bind_param('iiid',$idOrdine,$idProdotto,$quantita,$prezzo);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function getOrdiniAdmin($numElementi=25,$offset=0){
      $stmt = $this->db->prepare("(SELECT o.id as id, idCliente, data, SUM(quantita) as numArticoli,
                                  (SELECT SUM(prezzo*quantita) FROM righe_ordine ro
                                  WHERE idOrdine = o.id) as totale, stato
                                  FROM ordini o JOIN righe_ordine
                                  ON o.id = idOrdine
                                  WHERE stato = 'confermato'
                                  GROUP BY o.id
                                  ORDER BY stato,data)
                                  UNION
                                  (SELECT o.id as id, idCliente, data, SUM(quantita) as numArticoli,
                                  (SELECT SUM(prezzo*quantita) FROM righe_ordine ro
                                  WHERE idOrdine = o.id) as totale, stato
                                  FROM ordini o JOIN righe_ordine
                                  ON o.id = idOrdine
                                  WHERE stato = 'spedito'
                                  GROUP BY o.id
                                  ORDER BY data DESC)
                                  LIMIT ?,?");
      $stmt->bind_param('ii',$offset,$numElementi);
      return $this->executeSelect($stmt);
    }

    public function getNumOrdini(){
        $stmt = $this->db->prepare("SELECT COUNT(*) as numOrdini
                                      FROM ordini");
        return $this->executeSelect($stmt)[0]["numOrdini"];
    }

    public function spedisciOrdine($idOrdine){
      $stmt = $this->db->prepare("UPDATE ordini SET stato = 'spedito'
                                  WHERE id = ?");
      $stmt->bind_param('i',$idOrdine);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function getIdUtenteByOrdine($idOrdine){
      $stmt = $this->db->prepare("SELECT idCliente
                                    FROM ordini
                                    WHERE id=?");
      $stmt->bind_param('i',$idOrdine);
      return $this->executeSelect($stmt)[0]["idCliente"];
    }


  }
?>
