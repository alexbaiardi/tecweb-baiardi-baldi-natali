<?php
  class GestoreProdotti extends databaseHelper{

    public function getProdottiPiuVenduti($n=4){
        $stmt = $this->db->prepare("SELECT p.id, p.nome, p.prezzo, p.quantitaDisponibile, p.immagine, p.patentino
                                      FROM prodotti p JOIN righe_ordine ro
                                        ON p.id=ro.idProdotto
                                      WHERE p.cancellato = 0 AND p.quantitaDisponibile>0
                                      GROUP BY p.id
                                      ORDER BY sum(ro.quantita) DESC
                                      LIMIT ?");
        $stmt->bind_param('i',$n);
        return $this->executeSelect($stmt);
    }

    public function getNumProdottiByCategoria($idCategoria){
        $stmt = $this->db->prepare("SELECT COUNT(*) as numProdotti
                                      FROM prodotti
                                      WHERE idCategoria=? AND cancellato = 0");
        $stmt->bind_param('i',$idCategoria);
        return $this->executeSelect($stmt)[0]["numProdotti"];
    }

    public function getProdottiByCategoria($idCategoria,$numElementi=25,$offset=0){
        $stmt = $this->db->prepare("SELECT id, nome, prezzo, quantitaDisponibile, immagine, patentino
                                      FROM prodotti
                                      WHERE idCategoria=? AND cancellato = 0
                                      LIMIT ?,?");
        $stmt->bind_param('iii',$idCategoria,$offset,$numElementi);
        return $this->executeSelect($stmt);
    }

    public function getProdottoById($idProdotto){
        $stmt = $this->db->prepare("SELECT *
                                      FROM prodotti
                                      WHERE id=?  AND cancellato = 0 ");
        $stmt->bind_param('i',$idProdotto);
        return $this->executeSelect($stmt)[0];
    }

    public function getNumProdottiByNome($nomeProdotto){
        $stmt = $this->db->prepare("SELECT COUNT(*) as numProdotti
                                      FROM prodotti
                                      WHERE nome LIKE ?  AND cancellato = 0");
        $nomeProdotto="%".$nomeProdotto."%";
        $stmt->bind_param('s',$nomeProdotto);
        return $this->executeSelect($stmt)[0]["numProdotti"];
    }

    public function getProdottiByNome($nomeProdotto,$numElementi=25,$offset=0){
        $stmt = $this->db->prepare("SELECT *
                                      FROM prodotti
                                      WHERE nome LIKE ?  AND cancellato = 0
                                      LIMIT ?,?");
        $nomeProdotto="%".$nomeProdotto."%";
        $stmt->bind_param('sii',$nomeProdotto,$offset,$numElementi);
        return $this->executeSelect($stmt);
    }

    public function getNumProdottiByMarca($idMarca){
        $stmt = $this->db->prepare("SELECT COUNT(*) as numProdotti
                                      FROM prodotti
                                      WHERE idMarca=?  AND cancellato = 0 ");
        $stmt->bind_param('s',$idMarca);
        return $this->executeSelect($stmt)[0]["numProdotti"];
    }

    public function getProdottiByMarca($idMarca,$numElementi=25,$offset=0){
        $stmt = $this->db->prepare("SELECT *
                                      FROM prodotti
                                      WHERE idMarca=?  AND cancellato = 0
                                      LIMIT ?,?");
        $stmt->bind_param('iii',$idMarca,$offset,$numElementi);
        return $this->executeSelect($stmt);
    }

    public function getNumProdottiByMarcaNome($idMarca,$nomeProdotto){
        $stmt = $this->db->prepare("SELECT COUNT(*) as numProdotti
                                      FROM prodotti
                                      WHERE idMarca=? AND nome LIKE ?  AND cancellato = 0");
        $nomeProdotto="%".$nomeProdotto."%";
        $stmt->bind_param('is',$idMarca,$nomeProdotto);
        return $this->executeSelect($stmt)[0]["numProdotti"];
    }

    public function getProdottiByMarcaNome($idMarca,$nomeProdotto,$numElementi=25,$offset=0){
        $stmt = $this->db->prepare("SELECT *
                                      FROM prodotti
                                      WHERE idMarca=? AND nome LIKE ?  AND cancellato = 0
                                      LIMIT ?,?");
        $nomeProdotto="%".$nomeProdotto."%";
        $stmt->bind_param('isii',$idMarca,$nomeProdotto,$offset,$numElementi);
        return $this->executeSelect($stmt);
    }

    public function cancellaProdotto($idProdotto){
        $stmt = $this->db->prepare("UPDATE prodotti
                                    SET cancellato = 1
                                    WHERE id = ?");
        $stmt->bind_param('i',$idProdotto);
        $stmt->execute();
    }

    public function modificaProdotto($id,$nome,$descrizione,$prezzo,$quantitaDisponibile,$sitoWeb,$immagine,$patentino){
      $stmt = $this->db->prepare("UPDATE prodotti
                                  SET nome = ?, descrizione = ?, prezzo = ?,
                                  quantitaDisponibile = ?, sitoWeb = ?,immagine = ?, patentino = ?
                                  WHERE id = ?");
      $stmt->bind_param('ssdisssi',$nome,$descrizione,$prezzo,$quantitaDisponibile,$sitoWeb,$immagine,$patentino,$id);
      $stmt->execute();
    }

    public function aggiornaQuantita($quantita,$id){
      $stmt = $this->db->prepare("UPDATE prodotti
                                  SET quantitaDisponibile = quantitaDisponibile + ?
                                  WHERE id = ?");
      $stmt->bind_param('ii',$quantita,$id);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function aggiungiProdotto($nome,$descrizione,$prezzo,$quantitaDisponibile,$idCategoria,$idMarca,$sitoWeb,$immagine,$patentino,$cancellato){
      $stmt = $this->db->prepare("INSERT INTO prodotti(nome,descrizione,prezzo,quantitaDisponibile,idCategoria,idMarca,sitoWeb,immagine,patentino,cancellato)
                                  VALUES(?,?,?,?,?,?,?,?,?,?)");
      $stmt->bind_param('ssdiiissii',$nome,$descrizione,$prezzo,$quantitaDisponibile,$idCategoria,$idMarca,$sitoWeb,$immagine,$patentino,$cancellato);
      $stmt->execute();
      return $stmt->insert_id;
    }



  }
?>
