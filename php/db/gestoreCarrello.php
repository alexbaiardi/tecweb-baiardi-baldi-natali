<?php
require_once("databaseHelper.php");

  class GestoreCarrello extends DatabaseHelper{

    public function inserisciCarrello($carrello,$idCliente){
      foreach ($carrello as $idProdotto => $quantita) {
        $this->aggiungiProdotto($idProdotto,$quantita,$idCliente);
      }
    }

    public function aggiungiProdotto($idProdotto,$quantita,$idCliente){
      $stmt = $this->db->prepare("SELECT idProdotto, quantita
                                    FROM prodotti_carrello
                                    WHERE idCliente=? AND idProdotto=?");
      $stmt->bind_param('ii',$idCliente,$idProdotto);
      $result=$this->executeSelect($stmt);
      if(count($result)>0){
        $stmt = $this->db->prepare("UPDATE prodotti_carrello
                                      SET quantita= quantita + ?
                                      WHERE idCliente=? AND idProdotto=?");
        $stmt->bind_param('iii',$quantita,$idCliente,$idProdotto);
      } else {
        $stmt = $this->db->prepare("INSERT INTO prodotti_carrello (idProdotto,idCliente,quantita)
                                    VALUES (?,?,?)");
        $stmt->bind_param('iii',$idProdotto,$idCliente,$quantita);
      }
      $stmt->execute();
    }

    public function getCarrello($idCliente){
      $stmt = $this->db->prepare("SELECT idProdotto,quantita
                                    FROM prodotti_carrello
                                    WHERE idCliente=?");
      $stmt->bind_param('i',$idCliente);
      $result=$this->executeSelect($stmt);
      $carrello=array();
      foreach ($result as $riga) {
        $carrello[$riga["idProdotto"]]=$riga["quantita"];
      }
      return $carrello;
    }

    public function eliminaCarrello($idCliente){
      $stmt = $this->db->prepare("DELETE
                                    FROM prodotti_carrello
                                    WHERE idCliente=?");
      $stmt->bind_param('i',$idCliente);
      $stmt->execute();
    }

    public function modificaQuantitaProdotto($quantita,$idCliente,$idProdotto){
      $stmt = $this->db->prepare("UPDATE prodotti_carrello
                                    SET quantita= ?
                                    WHERE idCliente=? AND idProdotto=?");
      $stmt->bind_param('iii',$quantita,$idCliente,$idProdotto);
      $stmt->execute();
    }

    public function rimuoviProdotto($idCliente,$idProdotto){
      $stmt = $this->db->prepare("DELETE
                                    FROM prodotti_carrello
                                    WHERE idCliente=? AND idProdotto=?");
      $stmt->bind_param('ii',$idCliente,$idProdotto);
      $stmt->execute();
    }
  }
?>
