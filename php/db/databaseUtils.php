<?php
require_once("databaseHelper.php");

  class DatabaseUtils extends DatabaseHelper{


    public function getCategorie(){
        $stmt = $this->db->prepare("SELECT id, nome, immagine, anteprimaDescrizione
                                      FROM categorie");
        return $this->executeSelect($stmt);
    }

    public function getCategoriaById($idCategoria){
        $stmt = $this->db->prepare("SELECT *
                                      FROM categorie
                                      WHERE id=?");
        $stmt->bind_param('i',$idCategoria);
        return $this->executeSelect($stmt);
    }

    public function checkLogin($email, $password){
        $query = "SELECT id, email, nome, ruolo FROM utenti WHERE email = ? AND password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$email, $password);
        return $this->executeSelect($stmt);
    }

    public function getMarche(){
        $stmt = $this->db->prepare("SELECT * FROM marche");
        return $this->executeSelect($stmt);
    }

    public function getDatiAzienda(){
        $stmt = $this->db->prepare("SELECT * FROM azienda");
        return $this->executeSelect($stmt);
    }

    public function inserisciClientePatentino($email,$password,$ruolo,$patentino,$nome,$cognome,$idPatentino,$imgPatentino){
      $stmt = $this->db->prepare("INSERT INTO utenti (email,password,ruolo,patentino,nome,cognome,idPatentino,imgPatentino)
                                  VALUES (?,?,?,?,?,?,?,?)");
      $stmt->bind_param('sssissss',$email,$password,$ruolo,$patentino,$nome,$cognome,$idPatentino,$imgPatentino);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function inserisciClienteNoPatentino($email,$password,$ruolo,$patentino,$nome,$cognome){
      $stmt = $this->db->prepare("INSERT INTO utenti (email,password,ruolo,patentino,nome,cognome)
                                  VALUES (?,?,?,?,?,?)");
      $stmt->bind_param('sssiss',$email,$password,$ruolo,$patentino,$nome,$cognome);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function getDatiUtente($id){
      $stmt = $this->db->prepare("SELECT *
                                    FROM utenti
                                    WHERE id=?");
      $stmt->bind_param('i',$id);
      return $this->executeSelect($stmt)[0];
    }

    public function getIdVenditore(){
      $stmt = $this->db->prepare("SELECT id
                                    FROM utenti
                                    WHERE ruolo='venditore'");
      return $this->executeSelect($stmt)[0];
    }

    public function modificaDatiUtente($id,$email,$password,$patentino,$idPatentino,$imgPatentino){
      $stmt = $this->db->prepare("UPDATE utenti
                                  SET email = ?, password = ?, patentino = ?,
                                  idPatentino = ?, imgPatentino = ?
                                  WHERE id = ?");
      $stmt->bind_param('ssissi',$email,$password,$patentino,$idPatentino,$imgPatentino,$id);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function aggiornaIndirizzo($id,$indirizzo,$CAP){
      $stmt = $this->db->prepare("UPDATE utenti
                                  SET indirizzo = ?, CAP = ?
                                  WHERE id = ?");
      $stmt->bind_param('ssi',$indirizzo,$CAP,$id);
      $stmt->execute();

      return $stmt->insert_id;
    }

    public function aggiungiMarca($nome){
      $stmt = $this->db->prepare("INSERT INTO marche(nome)
                                  VALUES(?)");
      $stmt->bind_param('s',$nome);
      $stmt->execute();
      return $stmt->insert_id;
    }

  }
?>
