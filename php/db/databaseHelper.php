<?php

class DatabaseHelper{
  protected $db;

  public function __construct($servername, $email, $password, $dbname){
      $this->db = new mysqli($servername, $email, $password, $dbname);
      if ($this->db->connect_error) {
          die("Connection failed: " . $db->connect_error);
      }
  }

  protected function executeSelect($statement){
      $statement->execute();
      $result = $statement->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
  }
}
?>
