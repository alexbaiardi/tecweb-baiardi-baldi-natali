<?php
  require("utils/functions.php");
  require_once("setup.php");

  if(!isset($_REQUEST["idProdotto"]) || !isset($_REQUEST["quantita"])){
    header("Location: index.php");
  }

  $idProdotto=$_REQUEST["idProdotto"];
  $quantita=$_REQUEST["quantita"];

  if(isUserLoggedIn()){
    $dbCarrello->aggiungiProdotto($idProdotto,$quantita,$_SESSION["id"]);
    $carrello=$dbCarrello->getCarrello($_SESSION["id"]);
  } else{
    $cookie_name="carrello";
    if (!isset($_COOKIE[$cookie_name])) {
      $carrello=array($idProdotto => $quantita);
    } else {
      $carrello=json_decode($_COOKIE[$cookie_name],true);
      if(array_key_exists($idProdotto,$carrello)){
        $carrello[$idProdotto]+=$quantita;
      } else {
        $carrello[$idProdotto]= $quantita;
      }
    }
    setcookie($cookie_name,json_encode($carrello),time() + (60 * 60 * 24 * 30),"/");
  }
  
  echo json_encode(array('numProdottiCarrello' => array_reduce(array_values($carrello),"sum")));
 ?>
