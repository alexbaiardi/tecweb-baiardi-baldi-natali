<?php
  session_start();
  define("IMG_ICONE_DIR", "./img/icone/");
  define("IMG_CATEGORIE_DIR", "./img/categorie/");
  define("IMG_PRODOTTI_DIR", "./img/prodotti/");
  define("IMG_PATENTINI_DIR", "./img/patentini/");
  require_once("utils/functions.php");
  require_once("db/databaseUtils.php");
  require_once("db/gestoreProdotti.php");
  require_once("db/gestoreCarrello.php");
  require_once("db/gestoreOrdine.php");
  require_once("db/gestoreNotifiche.php");
  $dbNotifiche = new GestoreNotifiche("localhost", "root", "", "e-commerce_fitofarmaci");
  $dbProdotti = new GestoreProdotti("localhost", "root", "", "e-commerce_fitofarmaci");
  $dbOrdini = new GestoreOrdini("localhost", "root", "", "e-commerce_fitofarmaci");
  $dbCarrello= new GestoreCarrello("localhost", "root", "", "e-commerce_fitofarmaci");
  $dbh = new DatabaseUtils("localhost", "root", "", "e-commerce_fitofarmaci");
  $templateParams["categorie"]=$dbh->getCategorie();
  $datiAzienda=$dbh->getDatiAzienda()[0];
  if(isUserLoggedIn()){
    $nuoveNotifiche=$dbNotifiche->getNumNuoveNotificheByUtente($_SESSION["id"]);
    if($nuoveNotifiche>0){
      $templateParams["numNuoveNotifiche"]=$nuoveNotifiche;
    }
  }
 ?>
