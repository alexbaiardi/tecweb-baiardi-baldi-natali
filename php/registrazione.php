<?php
  require_once("setup.php");
  if(isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["nome"]) && isset($_POST["cognome"])){
    $hashedPassword=sha1($_POST["password"]);
    if($_POST["idPatentino"] != ""){
      $result = false;
      $errore = "";
      if ($_FILES["imgPatentino"]["size"] !== 0) {
        list($result, $msg) = uploadImage(IMG_PATENTINI_DIR, $_FILES["imgPatentino"]);
        if($result==1){
            $imgPatentino = $msg;
            $result=$dbh->inserisciClientePatentino($_POST["email"],$hashedPassword,"cliente",1,$_POST["nome"],$_POST["cognome"],
                                          $_POST["idPatentino"],$imgPatentino);
        }else {
          $errore = ": formato file non valido";
        }
      }else {
        $errore = ": inserisci il patentino";
      }
    } else {
      $result=$dbh->inserisciClienteNoPatentino($_POST["email"],$hashedPassword,"cliente",0,$_POST["nome"],$_POST["cognome"]);
    }
    if($result == false){
      $erroreInserimento = "Errore nell'inserimento".$errore;
      if ($errore == "") {
        unlink(IMG_PATENTINI_DIR.$imgPatentino);
      }
    }
    else{
        registerLoggedUser($result,"cliente");
        salvaCarrello();
    }
  }

  if (isUserLoggedIn()) {
    header("location: index.php");
  } else {
    $templateParams["titolo"]="Fitonline - Registrazione";
    $templateParams["mainTemplate"]="registrazione.php";
    $templateParams["js"]=array("js/datiUtente.js");
  }
  require 'template/base.php';
 ?>
