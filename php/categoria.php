<?php
  require_once("setup.php");

  if(isUserLoggedIn() && isUserAdmin()){
      $templateParams["js"]=array("js/modalOption.js");
  }else {
      $templateParams["js"]=array("js/quantita.js","js/aggiungiCarrello.js");
  }

  $templateParams["mainTemplate"]="categoria.php";
  $templateParams["asideTemplate"]="asideCatalogo.php";
  $templateParams["marche"]=$dbh->getMarche();

  define("MAX_ELEMENTI_PAGINA", 9);

  if(isset($_GET["idCategoria"])){
      $idCategoria = $_GET["idCategoria"];
      $categoria=$dbh->getCategoriaById($idCategoria);
      if(count($categoria)==0){
        header("Location: index.php");
      }
      $categoriaSelezionata=$categoria[0];
      $templateParams["titolo"]="Fitonline - ".$categoriaSelezionata["nome"];
  }
  else{
    header("Location: index.php");
  }

  if(isset($_GET["pagina"]) && is_numeric($_GET["pagina"])){
    $pagina=$_GET["pagina"];
  } else {
    $pagina=1;
  }

  $numProdotti=$dbProdotti->getNumProdottiByCategoria($idCategoria);
  list($paginaCorrente,$offset,$pagineTotali)=getPageOffsetTotalPage($pagina,MAX_ELEMENTI_PAGINA,$numProdotti);

  $templateParams["prodotti"]=$dbProdotti->getProdottiByCategoria($idCategoria,MAX_ELEMENTI_PAGINA,$offset);
  $templateParams["parametriRicerca"]="idCategoria=".$idCategoria;
  $templateParams["numRisultati"]=$numProdotti;
  $templateParams["paginaCorrente"]=$paginaCorrente;
  $templateParams["pagineTotali"]=$pagineTotali;

  require 'template/base.php';
 ?>
