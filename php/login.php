<?php
  require_once 'setup.php';

  if(isset($_POST["email"]) && isset($_POST["password"])){
    $hashedPassword = sha1($_POST["password"]);
    $login_result = $dbh->checkLogin($_POST["email"], $hashedPassword);
    if(count($login_result)==0){
      $errorelogin = "Errore! Controllare email o password!";
    }
    else{
      registerLoggedUser($login_result[0]["id"],$login_result[0]["ruolo"]);
      salvaCarrello();
    }
  }

  if(isUserLoggedIn()){
    if (isset($_COOKIE["login-carrello"]) && $_COOKIE["login-carrello"]==="true") {
      setcookie("login-carrello","",time() - 100,"/");
      header("location: carrello.php");
    }else {
      header("location: index.php");
    }
  }
  else{
    $templateParams["titolo"]="Fitonline - Login";
    $templateParams["mainTemplate"]="login.php";
  }

  require 'template/base.php';
 ?>
