<?php
  require_once("setup.php");
  $id = $_SESSION["id"];
  $idVenditore=$dbh->getIdVenditore()["id"];

  if(is_numeric($_POST["CAP"])){
    if ($_POST["metodo"] == "contrassegno" || ($_POST["numeroCarta"] !== "" && is_numeric($_POST["numeroCarta"])
    && ($_POST["scadenza"] !== "" &&(strtotime(date("Y-m")) < strtotime($_POST["scadenza"])))
    && ($_POST["cvv"] !== "" && is_numeric($_POST["cvv"])))){
      $carrello = $dbCarrello->getCarrello($id);
      $dbCarrello->eliminaCarrello($id);
      $data = date("Y-m-d");
      $idOrdine = $dbOrdini->inserisciOrdine($id,$data,"confermato");
      foreach ($carrello as $idProdotto => $quantita) {
        $prezzo = $dbProdotti->getProdottoById($idProdotto)["prezzo"];
        $dbProdotti->aggiornaQuantita(-$quantita,$idProdotto);
        if($dbProdotti->getProdottoById($idProdotto)["quantitaDisponibile"]==0){
          $dbNotifiche->generaNotificaProdottoEsaurito($idVenditore,$idProdotto);
        }
        $dbOrdini->inserisciRigaOrdine($idOrdine,$idProdotto,$quantita,$prezzo);
      }

      $dbNotifiche->generaNotificaNuovoOrdine($idVenditore,$idOrdine);
      $dbNotifiche->generaNotificaConfermaOrdine($id,$idOrdine);
      if (isset($_POST["salva"]) && $_POST["salva"] !== "") {
        $result=$dbh->aggiornaIndirizzo($id,$_POST["indirizzo"],$_POST["CAP"]);
      }
      header("location: ordini.php");
    } else {
      $errorePagamento = "ERRORE: I dati di pagamento non sono corretti";
      require 'cassa.php';
    }
  }else {
    $erroreIndirizzo = "ERRORE: CAP non valido";
    require 'cassa.php';
  }
 ?>
