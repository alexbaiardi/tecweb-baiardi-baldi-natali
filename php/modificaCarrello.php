<?php
  require("utils/functions.php");
  require_once("setup.php");

  if(!(isset($_REQUEST["idProdotto"])) || !(isset($_REQUEST["action"]))
        || ($_REQUEST["action"]!=="modifica" && $_REQUEST["action"]!=="rimuovi")
        || !isUserLoggedIn()){
    header("Location: index.php");
  }

  $idProdotto=$_REQUEST["idProdotto"];
  if($_REQUEST["action"]==="modifica"){
    if (isset($_REQUEST["quantita"])) {
      $tmpQuantita=$_REQUEST["quantita"];
    } else {
      header("Location: index.php");
    }
  }

  if($_REQUEST["action"]==="modifica"){
    $dbCarrello->modificaQuantitaProdotto($tmpQuantita,$_SESSION["id"],$idProdotto);
  } else {
    $dbCarrello->rimuoviProdotto($_SESSION["id"],$idProdotto);
  }

  $carrello=$dbCarrello->getCarrello($_SESSION["id"]);
  $subtotale=0;
  foreach ($carrello as $idProdotto => $quantita){
    $prodotto=$dbProdotti->getProdottoById($idProdotto);
    $subtotale+=$prodotto["prezzo"]*$quantita;
  }

  echo json_encode(array("numProdotti" => getNumProdottiCarrello(), "subtotale"=>number_format($subtotale,2,"."," ")));
 ?>
