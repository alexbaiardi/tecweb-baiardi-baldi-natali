<?php
  require_once("setup.php");

  if(!(isUserLoggedIn() && isUserAdmin())){
    header("index.php");
  }

  define("MAX_ELEMENTI_PAGINA", 15);
  if(isset($_GET["pagina"]) && is_numeric($_GET["pagina"])){
    $pagina=$_GET["pagina"];
  } else {
    $pagina=1;
  }

  $numOrdini=$dbOrdini->getNumOrdini();
  list($paginaCorrente,$offset,$pagineTotali)=getPageOffsetTotalPage($pagina,MAX_ELEMENTI_PAGINA,$numOrdini);

  if (isset($_POST["idOrdine"])) {
    $dbOrdini->spedisciOrdine($_POST["idOrdine"]);
    $dbNotifiche->generaNotificaSpedizioneOrdine($dbOrdini->getIdUtenteByOrdine($_POST["idOrdine"]),$_POST["idOrdine"]);
  }

  $datiOrdini = $dbOrdini->getOrdiniAdmin(MAX_ELEMENTI_PAGINA,$offset);

  $templateParams["titolo"]="Fitonline - Ordini";
  $templateParams["mainTemplate"]="ordiniAdmin.php";
  $templateParams["js"]=array("js/ordini.js");
  $templateParams["numRisultati"]=$numOrdini;
  $templateParams["paginaCorrente"]=$paginaCorrente;
  $templateParams["pagineTotali"]=$pagineTotali;
  require 'template/base.php';
 ?>
