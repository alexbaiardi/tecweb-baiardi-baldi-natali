$(document).ready(function(){
  const bottone = $("button.dettaglio");
  $("div.visualizzaDettaglio").hide();
  bottone.click(function(){
    const dettaglio = $(this).parents("div.riepilogo").siblings("div.visualizzaDettaglio");
    if (dettaglio.is(":visible")){
      dettaglio.hide();
      $(this).text("Mostra dettaglio");
    } else {
      dettaglio.show();
      $(this).text("Chiudi dettaglio");
    }
  });
});
