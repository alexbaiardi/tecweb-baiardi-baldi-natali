$(document).ready(function() {
  $(".modifica-quantita").click(function() {
    const form=$(this).parents("form");
    const url = form.attr("action");
    const metodo = form.attr("method");
    const idProdotto=form.children("[name='idProdotto']").val();
    const quantita=$(this).siblings("[type='number']").val();
    const action="modifica";
    const dati_form={"idProdotto": idProdotto,"quantita":quantita, "action":action};
    const thisButton=$(this);
    $.ajax({
      url: url,
      type: metodo,
      data: dati_form,
      success: function(response){
        thisButton.blur();
        const jsonData=JSON.parse(response);
        $("#badge-carrello").text(jsonData["numProdotti"]);
        $("#subtotale").text(jsonData["subtotale"]+ " €");
      }
    });
  });

  $(".rimuovi").submit(function(event) {
    event.preventDefault();
    const url = $(this).attr("action");
    const metodo = $(this).attr("method");
    const idProdotto=$(this).children("[name='idProdotto']").val();
    const quantita=$(this).siblings("[type='number']").val();
    const action="rimuovi";
    const dati_form={"idProdotto": idProdotto, "action":action};
    const button=$(this).children(":submit");
    const rigaCarrello=$(this).parents(".riga-carrello");
    $.ajax({
      url: url,
      type: metodo,
      data: dati_form,
      success: function(response){
        button.blur();
        const jsonData=JSON.parse(response);
        $("#badge-carrello").text(jsonData["numProdotti"]);
        $("#subtotale").text(jsonData["subtotale"]+ " €");
        rigaCarrello.remove();
        if(jsonData["numProdotti"]==null){
          window.location.replace("index.php");
        }
      }
    });
  });
});
