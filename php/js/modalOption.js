$(document).ready(function(){
  const exampleModal = $("#modalCancelProduct");
  exampleModal.on('show.bs.modal', function (event) {
    const button = event.relatedTarget;
    const recipient = button.getAttribute('data-bs-whatever');
    $("#cancelProduct").attr('href', "./operazioniProdotto.php?action=1&id="+recipient);
  });
});
