$(document).ready(function functionName() {

  let visible = true;
  const pagamento = $("div#pagamento");

  $("#carta").prop("checked", true);

  $("#carta").change(function(){
    pagamento.show();
    visible=true;
    $("input#metodo").val("carta");
  });

  $("#contrassegno").change(function(){
    pagamento.hide();
    visible=false;
    $("input#metodo").val("contrassegno");
  });

});
