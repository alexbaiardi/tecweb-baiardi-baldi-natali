const pattern = /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i;

$(document).ready(function(){

  let visible=false;
  const input_email = $("input#email");
  const input_password = $("input#password");
  const input_emailConf = $("input#emailConf");
  const input_passwordConf = $("input#passwordConf");
  const patFields = $("div#numPat, div#uploadPat");
  const input_numPat = $("input#idPat");
  const input_filePat = $("input#pat");

  patFields.hide();

  $("#si").change(function(){
    patFields.show();
    visible=true;
  });

  $("#no").change(function(){
    patFields.hide();
    input_numPat.val("");
    visible=false;
  });

  $("form#registrazione").submit(function(e){
      e.preventDefault();
      let isFormOk = true;

      input_password.next().remove();
      if(!pattern.test(input_password.val())){
          input_password.parent().append("<p>La password deve contenere almeno un numero e una lettera</p>");
          isFormOk = false;
      }

      if(input_emailConf.val() != input_email.val()){
        input_emailConf.parent().append("<p>Errore nella conferma della mail</p>")
        isFormOk = false;
      }

      if(input_passwordConf.val() != input_password.val()){
        input_passwordConf.parent().append("<p>Errore nella conferma della password</p>")
        isFormOk = false;
      }

      if(visible && (input_numPat.val().length == 0 && input_filePat.val().length == 0)){
        input_numPat.parent().append("<p>Errore, il numero di patentino non può essere nullo</p>");
        input_filePat.parent().append("<p>Errore, carica una copia del tuo patentino</p>");
        isFormOk = false;
      }

      if(isFormOk){
          e.currentTarget.submit();
      }
  })

  $("input#modificaPat").change(function(){
    if ($(this).is(':checked')) {
      patFields.show();
    } else {
      patFields.hide();
    }
  });

  $("form#modifica").submit(function(e){
      e.preventDefault();
      let isFormOk = true;

      input_password.next().remove();
      if((!pattern.test(input_password.val()) && input_password.val().length < 8)
          && input_password.val().length != 0){
          input_password.parent().append("<p>La password deve contenere almeno un numero e una lettera</p>");
          isFormOk = false;
      }

      if(input_emailConf.val() != input_email.val()){
        input_emailConf.parent().append("<p>Errore nella conferma della mail</p>")
        isFormOk = false;
      }

      if(input_passwordConf.val() != input_password.val()){
        input_passwordConf.parent().append("<p>Errore nella conferma della password</p>")
        isFormOk = false;
      }

      if(isFormOk){
          e.currentTarget.submit();
      }
  })
});
