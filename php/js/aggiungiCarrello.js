$(document).ready(function(){
    $("form.carrello").submit(function(e){
      e.preventDefault();
    	const url = $(this).attr("action");
    	const metodo = $(this).attr("method");
    	const dati_form = $(this).serialize();

      const submit=$(this).find(":submit");
      const divInfo=$(this).find(".info");
      const alertSuccess="<span class=\"alert alert-success delay-alert\" role=\"alert\"> Aggiunto al carrello! </span>";
      $.ajax({
    		url: url,
    		type: metodo,
    		data: dati_form,
        success: function(response){
          const jsonData=JSON.parse(response);
          $("#badge-carrello").text(jsonData["numProdottiCarrello"]);
          submit.blur();
          divInfo.append(alertSuccess);
          $(".delay-alert").delay(1500).fadeOut();
      	}
      });
    });
});
