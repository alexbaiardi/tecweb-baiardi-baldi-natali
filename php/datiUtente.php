<?php
  require_once("setup.php");
  $datiUtente = $dbh->getDatiUtente($_SESSION["id"]);
  $templateParams["mainTemplate"]="datiUtente.php";
  $templateParams["titolo"]="Fitonline - I miei dati";

  if (isset($_POST["modifica"])) {
    if($_POST["modifica"] == "si"){
      $templateParams["mainTemplate"] = "modificaDati.php";
    } else {
      $password=$datiUtente["password"];
      $email=$datiUtente["email"];
      $patentino=$datiUtente["patentino"];
      $idPatentino=$datiUtente["idPatentino"];
      $imgPatentino=$datiUtente["imgPatentino"];

      if ($_POST["password"] != "") {
        $password=$_POST["password"];
        $password=sha1($password);
      }
      if ($_POST["email"] != "") {
        $email=$_POST["email"];
      }

      if(($_POST["idPatentino"] != $idPatentino)){
        if ($_POST["idPatentino"] == "") {
          $idPatentino=null;
          $patentino=0;
          $imgPatentino=null;
        }else {
          $patentino=1;
          $idPatentino=$_POST["idPatentino"];
          unlink(IMG_PATENTINI_DIR.$imgPatentino);
          list($result, $msg) = uploadImage(IMG_PATENTINI_DIR, $_FILES["imgPatentino"]);
          if($result==1){
              $imgPatentino = $msg;
          }
        }
      }
      $result=$dbh->modificaDatiUtente($datiUtente["id"],$email,$password,$patentino,$idPatentino,$imgPatentino);
      if ($result==false) {
        $erroreAggiornamento = "ERRORE: Aggiornamento non riuscito";
      }
    }
  }

  $datiUtente = $dbh->getDatiUtente($_SESSION["id"]);
  $templateParams["js"]=array("js/datiUtente.js");
  require 'template/base.php';
 ?>
