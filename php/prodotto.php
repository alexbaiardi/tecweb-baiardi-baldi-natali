<?php
  require_once("setup.php");

  $condizione = (isset($erroreAggiunta)) ? $erroreAggiunta : true ;
  if(isUserLoggedIn() && isUserAdmin() && isset($_GET["action"]) && $_GET["action"] == 3 && $condizione  ){
    $templateParams["titolo"]="Fitonline - Inserisci prodotto";
    $templateParams["mainTemplate"]="aggiuntaProdotto.php";
    }else{
      if (isUserLoggedIn() && isUserAdmin() ){
        $templateParams["mainTemplate"]="paginaProdottoAdmin.php";
      }else{
        $templateParams["js"]=array("js/quantita.js","js/aggiungiCarrello.js");
        $templateParams["mainTemplate"]="paginaProdotto.php";
      }
      if(isset($checkOperazioneProd)){
        $idProdotto = $checkOperazioneProd;
      }else{
        if(isset($_GET["id"])){
          $idProdotto = $_GET["id"];
        }
      }
      $prodottoSelezionato=$dbProdotti->getProdottoById($idProdotto);
      $templateParams["titolo"]="Fitonline - ".$prodottoSelezionato["nome"];
  }

  $templateParams["marche"]=$dbh->getMarche();
  $templateParams["categorie"]=$dbh->getCategorie();
  $templateParams["asideTemplate"]="asideCatalogo.php";
  require 'template/base.php';
 ?>
