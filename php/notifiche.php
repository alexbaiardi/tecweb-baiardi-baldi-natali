<?php
  require_once("setup.php");

  if(!isUserLoggedIn()){
    header("location: index.php");
  }
  $templateParams["titolo"]="Fitonline - Notifiche";
  $templateParams["mainTemplate"]="notifiche.php";
  $templateParams["js"]=array("js/notifiche.js");
  $templateParams["notifiche"] = $dbNotifiche->getNotificheByUtente($_SESSION["id"]);
  require 'template/base.php';
 ?>
